import zmq
import time
import sys


host="127.0.0.1"
port="3000"
if len(sys.argv)>1:
    host=sys.argv[1]
if len(sys.argv)>2:
    port=sys.argv[2]

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('tcp://'+host+':'+port)
print("Accepting clients...")

game_state="game_state"
start_time = time.time()
actions=""
while True:

    action = socket.recv_pyobj()
    print("Received: ", action)
    actions+='_'+action
    socket.send_pyobj(game_state)
    
    elapsed_time = time.time() - start_time
    if elapsed_time>2:
        print("update state: ",actions)
        start_time = time.time()
        game_state+=actions
        actions=""
