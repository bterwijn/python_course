import time
import zmq
context = zmq.Context()

socket_game_state = context.socket(zmq.PUB)
socket_game_state.bind('tcp://127.0.0.1:2000')
print("socket_game_state bound")

socket_actions = context.socket(zmq.SUB)
socket_actions.setsockopt_string(zmq.SUBSCRIBE, '')
socket_actions.bind('tcp://127.0.0.1:2020')
print("socket_actions bound")

count=0
while True:
    #time.sleep(0.5)
    
    print("receiving action")
    action = socket_actions.recv_pyobj()
    print("action:",action)

    print("sending game_state:",count)
    socket_game_state.send_pyobj(count)

    count+=100
