import zmq
import time
import sys

name="unkown"
host="127.0.0.1"
port="3000"

if len(sys.argv)>1:
    name=sys.argv[1]
if len(sys.argv)>2:
    host=sys.argv[2]
if len(sys.argv)>3:
    port=sys.argv[3]
    
context = zmq.Context()
print("Connecting to server...")
socket = context.socket(zmq.REQ)
socket.connect ('tcp://'+host+':'+port)

action=0
while True:
    print("Sending:", action)
    socket.send_pyobj(name+str(action))
    message = socket.recv_pyobj()
    print("Received:", message)
    action+=1
    time.sleep(0.5)
