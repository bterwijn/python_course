import time
import zmq
import sys
context = zmq.Context()

name="unkown"
if len(sys.argv)>1:
    name=sys.argv[1]

socket_game_state = context.socket(zmq.SUB)
socket_game_state.setsockopt_string(zmq.SUBSCRIBE, '')
socket_game_state.connect('tcp://127.0.0.1:2000')
print("socket_game_state connected")

socket_actions = context.socket(zmq.PUB)
socket_actions.connect('tcp://127.0.0.1:2020')
print("socket_actions connected")

count=0
while True:
    time.sleep(0.5)
    
    print("sending action:",count)
    socket_actions.send_pyobj(name+"_"+str(count))

    print("receiving game_state")
    game_state = socket_game_state.recv_pyobj()
    print("game_state:",game_state)

    count+=1
