'''Double the value values

Write a function that returns a list in wich all the values in
'numbers' are doubled.

'''

def double_0(numbers):
    result=[]
    for i in range(len(numbers)):
        result.append(numbers[i]*2)
    return result

def double_1(numbers):
    result=[]
    for i in numbers:
        result.append(i*2)
    return result

def double_2(numbers):
    result=[i*2 for i in numbers]
    return result

def double_3(numbers):
    return [i*2 for i in numbers]

def main():
    numbers=[6,4,7,5,25,10,36,331]
    print("numbers: ",numbers)

    numbers_doubled=double_0(numbers)
    print("double_0: ",numbers_doubled)
    print("double_1: ",double_1(numbers))
    print("double_2: ",double_2(numbers))
    print("double_3: ",double_3(numbers))
    
main()
