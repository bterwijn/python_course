'''Sorting

Write a function to sort the numbers in 'numbers' using the "Bubble
sort" algorithm.

'''

def bubble_sort_iteration(numbers,length):
    swap_count=0
    print("swappping: ",end='')
    for i in range(length):        
        if numbers[i]>numbers[i+1]:
            swap_count+=1
            temp=numbers[i]
            numbers[i]=numbers[i+1]
            numbers[i+1]=temp
            print(i,'',end='')
    print()
    return swap_count
        
def bubble_sort(numbers):
    length=len(numbers)-1
    while bubble_sort_iteration(numbers,length)>0:
        print("bubble_sort: ",numbers)
        length-=1

def main():
    numbers=[10,5,9,3,14,6,31,3,8,7,9,2]
    print("numbers: ",numbers)
    
    bubble_sort(numbers)
    print("numbers sorted: ",numbers)

    
main()
