'''Sneew

Maak een bord met een breedte 3 en hoogte 4 met '.' characters. En
schrijf een print functie om het bord weer te geven als:

...
...
...
...

Schrijf een functie die characters op de bovenste regel met een kans
van 0.05 vervangt met character '*', een sneewvlok. Dus bijvoorbeeld:

*.*
...
...
...

Schrijf een functie die elke sneeuwvlok een positie naar beneden
verplaatst als daar een '.' character staat.

...
*.*
...
...

Gebruik deze functies om een sneeuw-animatie weer te geven.
Handige functie hierbij zijn:

import random
print(random.random()) # random number between 0 and 1

import time
time.sleep(0.1) # stop program execution for 0.1 seconds

'''
import random
import time


def main():
    width = 40
    height = 20
    empty_cell = '.'
    snow_cell = '*'
    snow_chance = 0.05
    board = empty_board(width, height, empty_cell)
    print_board(board)
    while True:
        move_snow(board, empty_cell)
        random_snow(board, snow_cell, snow_chance)
        print_board(board)
        time.sleep(0.1)


def empty_board(width, height, empty_cell):
    """ Returns a two dimensional array of size 'width' by 'height' filled
    with value 'empty_cell'.
    """
    return [[empty_cell for i in range(width)] for i in range(height)]


def print_board(board):
    """ Prints the values of two dimensional array 'board'.
    """
    print()
    for row in board:
        for cell in row:
            print(cell, end='')
        print()


def random_snow(board, snow_cell, snow_chance):
    """ With a chance of 'snow_chance' adds value 'snow_cell' to the elements
    in the first row of two dimensional array 'board'.
    """
    width = len(board[0])
    for x in range(width):
        if random.random() < snow_chance:
            board[0][x] = snow_cell


def move_snow(board, empty_cell):
    """ Move any value other then 'empty_cell' one row down in two
    dimensional array 'board' if that spot exists and has value 'empty_cell'.
    """
    height = len(board)
    width = len(board[0])
    for y in range(height - 2, -1, -1):
        for x in range(width):
            if board[y][x] != empty_cell:
                ny = y + 1
                if board[ny][x] == empty_cell:
                    board[ny][x] = board[y][x]
                    board[y][x] = empty_cell


main()
