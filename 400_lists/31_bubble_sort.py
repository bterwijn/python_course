'''Sorting

Write a function to sort the names in 'names' using the "Bubble
sort" algorithm.

'''

def bubble_sort_iteration(numbers,length):
    swap_count=0
    print("swappping: ",end='')
    for i in range(length):        
        if numbers[i]>numbers[i+1]:
            swap_count+=1
            temp=numbers[i]
            numbers[i]=numbers[i+1]
            numbers[i+1]=temp
            print(i,'',end='')
    print()
    return swap_count
        
def bubble_sort(numbers):
    length=len(numbers)-1
    while bubble_sort_iteration(numbers,length)>0:
        print("bubble_sort: ",numbers)
        length-=1

def main():
    names=["Liam","Olivia","Noah","Emma","Oliver","Ava","William","Sophia"]
    print("names: ",names)
    
    bubble_sort(names)
    print("names sorted: ",names)

    
main()
