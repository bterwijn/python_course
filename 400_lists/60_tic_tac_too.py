
empty_cell='.'

def ask_board_length():
    while True:
        try:
            board_length=int(input("Give the board length: "))
            if board_length>=0:
                break
        except ValueError:
            pass
        print("invalid length")
    return board_length

def empty_board(board_length):
    return [[empty_cell for i in range(board_length)] for i in range(board_length)]
    
def print_board(board):
    for row in board:
        for cell in row:
            print(cell,end='')
        print()

def ask_players():
    return input("Type a character for each player that is in the game and enter: ")

def ask_coordinate(board,axis):
    while True:
        try:
            coordinate=int(input("Give the "+axis+" coordinate (0 through "+str(len(board)-1)+"): "))
            if coordinate>=0 and coordinate<len(board):
                break
        except ValueError:
            pass
        print("invalid coordinate")
    return coordinate
    
def ask_move(board):
    while True:
        x=ask_coordinate(board,'X')
        y=ask_coordinate(board,'Y')
        if board[y][x]==empty_cell:
            break
        print("invalid move, already occupied")
    return x,y
        
def move(board,x,y,player):
    board[y][x]=player
    
def check_win_line(board,x,y,dx,dy):
    player=board[y][x]
    if player==empty_cell:
        return None
    for i in range(len(board)-1):
        x+=dx
        y+=dy
        p=board[y][x]
        if p!=player:
            return None
    return player
    
def check_win(board):
    # horizontal
    for x in range(len(board)):
        player=check_win_line(board,x,0,0,1)
        if not player is None:
            return player
    # vertical
    for y in range(len(board)):
        player=check_win_line(board,0,y,1,0)
        if not player is None:
            return player
    # diagonal
    player=check_win_line(board,0,0,1,1)
    if not player is None:
        return player
    player=check_win_line(board,len(board)-1,0,-1,1)
    if not player is None:
        return player
    return None
    
def main():
    board_length=ask_board_length()
    board=empty_board(board_length)
    print_board(board)
    players=ask_players()
    winner=None
    i=0
    while winner is None:
        p=players[i%len(players)]
        print("Player",p)
        i+=1
        x,y=ask_move(board)
        move(board,x,y,p)
        print_board(board)
        winner=check_win(board)
        if not winner is None:
            break
    print("Player",winner,"won!")
        
main()
