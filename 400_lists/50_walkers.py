import random
import time

empty_cell=0

def empty_board(width):
    return [empty_cell for i in range(width)]

def add_value_at_random(board,value):
    width=len(board)
    while True:
        rx=random.randint(0,width-1)
        if board[rx]==empty_cell:
            board[rx]=value
            break

def add_value_at_random_mutiple_times(board,value,n):
    for i in range(n):
        add_value_at_random(board,value)
        
def print_board(board):
    for cell in board:
        if cell>0:
            print(cell,end='')
        else:
            print('.',end='')
    print()

def walk(board,new_board,x):
    width=len(board)
    nx=(x+random.choice([1,-1]))%width
    new_board[nx]=board[x]+board[nx]+new_board[nx]
    board[x]=empty_cell
    board[nx]=empty_cell

def next_step(board):
    width=len(board)
    new_board=empty_board(width)
    for x in range(width):
        if board[x]!=empty_cell:
            walk(board,new_board,x)
    return new_board

def main():
    width=60
    board=empty_board(width)
    add_value_at_random_mutiple_times(board,1,20)
    print_board(board)
    while True:
         board=next_step(board)
         print_board(board)
         time.sleep(0.1)
         
main()
