'''Average grade

Write a function that computes the average grade of 'grades' where
each grade is weigthed by the correspnding value in 'weights'. So
grade 6 counts 1 time, grade 4 counts 2 times, etc.

'''

def compute_grade(grades,weights):
    sum_grades=0
    sum_weights=0
    for index,weight in enumerate(weights):
        sum_weights+=weight
        sum_grades+=weight*grades[index]
    return sum_grades/sum_weights

def round_grade(grade):
    return round(grade*2)/2

def main():
    grades=[6,4,7,5]
    weights=[1,2,1.5,2]
    print( round_grade( compute_grade(grades,weights) ) )
    
main()
