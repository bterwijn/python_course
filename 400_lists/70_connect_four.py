import random

def main():
    width = 7
    height = 6
    empty_cell = '.'
    players = [('Player1', 'X'), ('Player2', 'O')]
    move_count = 0
    board = make_board(width, height, empty_cell)
    print_board(board)
    move_list=[]
    while True:
        player_name=players[move_count%2][0]
        player_cell=players[move_count%2][1]
        move=ask_next_move(player_name,board,empty_cell)
        #move=random_next_move(player_name,board,empty_cell)
        move_list.append(move)
        move_on_row = do_move(move,board,empty_cell,player_cell)
        move_count+=1
        #print(move_count)
        print_board(board)
        if winning_move((move_on_row,move-1),board,player_cell):
            print("WINNER:",player_name)
            break
        if move_count == width*height:
            print("DRAW")
            break
    #print_moves(move_list)

def print_moves(move_list):
    for i in move_list:
        print(i)

def make_board(width, height, empty_cell):
    return [[empty_cell for col in range(width)] for row in range(height)]

def print_board(board):
    """ Prints the 'board'. """
    print()
    for i in range(1,len(board[0])+1):
        print(' ', i, end='')
    print()
    for row in board:
        for cell in row:
            print(' ', cell, end='')
        print()
        
def input_integer(question):
    while True:
        try:
            integer=int(input(question))
            break
        except ValueError:
            print('not and integer :(')
    return integer

def valid_move(move,board,empty_cell):
    width=len(board[0])
    if move<=0 or move>width:
        return False
    if board[0][move-1]!=empty_cell:
        return False
    return True

def do_move(move,board,empty_cell,player_cell):
    height=len(board)
    for row in range(height-1):
        if board[row+1][move-1]!=empty_cell:
            board[row][move-1]=player_cell
            return row
    board[height-1][move-1]=player_cell
    return height-1

def random_next_move(player,board,empty_cell):
    while True:
        move=1+random.randrange(len(board[0]))
        if valid_move(move,board,empty_cell):
            break
        else:
            print('invalid move :(')
    return move

def ask_next_move(player,board,empty_cell):
    while True:
        move=input_integer(""+player+", type your move: ")
        if valid_move(move,board,empty_cell):
            break
        else:
            print('invalid move :(')
    return move

def count_cells_one_direction(pos,board,cell,step):
    count=0
    row=pos[0]
    column=pos[1]
    height=len(board)
    width=len(board[0])
    while True:
        if row<0 or row>=height:
            break
        if column<0 or column>=width:
            break
        if board[row][column]!=cell:
            break
        #print("row:",row,"column:",column)
        count+=1
        row+=step[0]
        column+=step[1]
    return count

def count_cells_both_directions(pos,board,cell,step):
    count=count_cells_one_direction(pos,board,cell,step)
    negative_step=(-step[0],-step[1])
    count+=count_cells_one_direction(pos,board,cell,negative_step)
    return count-1
    
def winning_move(pos,board,cell):
    for step in [(0,1),(1,0),(1,1),(1,-1)]: # horizontal, vertical, bottom_right_diagonal, bottom_left_diagonal
        count = count_cells_both_directions(pos,board,cell,step)
        if (count>=4):
            return True
    return False
    
main()
