import random
import time

empty_cell=0

def empty_board(width,height):
    return [[empty_cell for i in range(width)] for i in range(height)]

def add_value_at_random(board,value):
    height=len(board)
    width=len(board[0])
    while True:
        ry=random.randint(0,height-1)
        rx=random.randint(0,width-1)
        if board[ry][rx]==empty_cell:
            board[ry][rx]=value
            break

def add_value_at_random_mutiple_times(board,value,n):
    for i in range(n):
        add_value_at_random(board,value)
        
def print_board(board):
    width=len(board[0])
    print("="*width)
    for row in board:
        for cell in row:
            if cell>0:
                print(cell,end='')
            else:
                print('.',end='')
        print()

def walk(board,new_board,y,x):
    height=len(board)
    width=len(board[0])
    ny=y
    nx=x
    if random.choice([True, False]):
        ny=(ny+random.choice([1,-1]))%height
    else:
        nx=(nx+random.choice([1,-1]))%width
    new_board[ny][nx]=board[y][x]+board[ny][nx]+new_board[ny][nx]
    board[y][x]=empty_cell
    board[ny][nx]=empty_cell
    
def next_step(board):
    height=len(board)
    width=len(board[0])
    new_board=empty_board(width,height)
    for y in range(height):
        for x in range(width):
            if board[y][x]!=empty_cell:
                walk(board,new_board,y,x)
    return new_board

def main():
    width=20
    height=10
    board=empty_board(width,height)
    add_value_at_random_mutiple_times(board,1,90)
    print_board(board)
    while True:
         board=next_step(board)
         print_board(board)
         time.sleep(0.1)
         
main()
