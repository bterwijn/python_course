'''Sorting

Write a function that generates a list of 'n' random numbers in range
['min','max']. Then sort these number using your bubble sort and
compare that with sorting using Python's built-in sort() function.

'''

import random

def random_ints(n,min,max):
    return [random.randint(min,max) for i in range(n)]

def bubble_sort_iteration(numbers,length):
    swap_count=0
    for i in range(length):        
        if numbers[i]>numbers[i+1]:
            swap_count+=1
            temp=numbers[i]
            numbers[i]=numbers[i+1]
            numbers[i+1]=temp
    return swap_count
        
def bubble_sort(numbers):
    length=len(numbers)-1
    while bubble_sort_iteration(numbers,length)>0:
        length-=1

def main():
    n=10000
    min=0
    max=n*10
    numbers=random_ints(n,min,max)
    print("numbers: ",numbers)
    
    bubble_sort(numbers)
    #numbers.sort() # much faster! use library functions: faster,easier,less bugs
    print("numbers sorted: ",numbers)
    
main()
