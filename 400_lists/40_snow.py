'''Snow

'''
import random
import time

empty_cell='.'
snow='*'

def empty_board(length):
    return [empty_cell for i in range(length)]

def print_board(board):
    print("=")
    for cell in board:
        print(cell)

def random_snow(board):
    if random.random()<0.05:
        board[0]=snow

def next_step(board):
    length=len(board)
    new_board=empty_board(length)
    for i in range(length):
        if board[i]==snow:
            ni=i+1
            if ni<length and board[ni]==empty_cell:
                new_board[ni]=snow
            else:
                new_board[i]=snow
    return new_board

def main():
    length=30
    board=empty_board(length)
    print_board(board)
    while True:
        board=next_step(board)
        random_snow(board)
        print_board(board)
        time.sleep(0.1)

main()
