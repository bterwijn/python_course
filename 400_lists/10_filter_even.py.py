'''Double the value values

Write a function that returns a list of all the values in
'numbers' that are even (multiples of 2).

'''

def filter_even_1(numbers):
    result=[]
    for i in numbers:
        if i%2==0:
            result.append(i)
    return result

def filter_even_2(numbers):
    result=[i for i in numbers if i%2==0]
    return result

def filter_even_3(numbers):
    return [i for i in numbers if i%2==0]

def main():
    numbers=[6,4,7,5,25,10,36,331]
    print("numbers: ",numbers)
    
    numbers_filtered=filter_even_1(numbers)
    print("filter_even_1: ",numbers_filtered)
    print("filter_even_2: ",filter_even_2(numbers))
    print("filter_even_3: ",filter_even_3(numbers))
    
main()
