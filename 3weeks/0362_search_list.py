import random
import time
    
def main():
    random.seed(0)
    n=20
    container=make_container(n)
    print(container)
    container.sort()
    print(container)
    search_elements(n,container)
    
def random_element(n):
    return random.randint(0,n)

def make_container(n):
    return [ random_element(n) for i in range(n) ] # list

def search_elements(n,container,tries=1000):
    start_time=time.time()
    count=0
    for i in range(tries):
        element=random_element(n)
        if element in container:
            count+=1
    stop_time=time.time()
    print("average search time:",(start_time-stop_time)/tries)
    print(f"found: {100*count/tries}%")

main()
