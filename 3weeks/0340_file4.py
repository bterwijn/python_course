    
with open('test.txt', 'w') as file1:
    file1.write('0    John        12\n')
    file1.write('1   Mary        14\n')
    file1.write('2       Peter    11\n')
    
total=0
with open('test.txt', 'r') as file2:
    for line in file2:
        elements = [element.strip() for element in line.split()]
        print(elements)
        try:
            total+=float(elements[2])
        except ValueError:
            print(f"'{elements[2]}' is not a float on line: {line}")
# ['0', 'John', '12']
# ['1', 'Mary', '14']
# ['2', 'Peter', '11']
print(total) # 37.0
