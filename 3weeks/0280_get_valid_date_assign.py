def main():
    year=ask_user_int("Which year? ",lambda year: True)
    month=ask_user_int("Which month (1-12)? ",is_valid_month)
    days_in_month_year=compute_days_in_month_year(year,month)
    day=ask_user_int(f"Which day of the month (1-{days_in_month_year})? ",lambda day: day>=1 and day<=days_in_month_year)
    print(f"year:{year} month:{month} day:{day}")
    
def ask_user_int(question,validator_function):
    while True:
        try:
            user_int=int(input(question))
            if validator_function(user_int):
                break
            else:
                print("invalid, not a valid value")
        except ValueError:
            print("invalid, not an integer")
    return user_int

def is_valid_month(month):
    return month>=1 and month<=12

def is_leap_year(year):
    leap_year=False
    if year%4:            # every fourth year is a leap year, except when
        if year%100:      # it is devisible by 100, with an exception
            if year%400:  # for years devisible by 400
                leap_year=True
        else:
            leap_year=True
    return leap_year

def compute_days_in_month_year(year,month):
    if month in [4,6,9,11]: # April, June, September, and November have 30 days
        return 30
    if month==2: # Februari has 28 or 29 days depending on leap year
        if is_leap_year(year): 
            return 29
        else:
            return 28
    return 31 # all other months have 31 days
    
main()
