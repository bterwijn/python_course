import copy

def main():
    permutation_list = get_permutation_list(3, ['Y', 'N'])
    permutation_list_renamed = rename_permutation_list(permutation_list, 'N', 'n')
    print(permutation_list)         # [[], ['Y'], ['N'], ['Y', 'Y'], ['Y', 'N'], ['N', 'Y'], ['N', 'N'], ['Y', 'Y', 'Y'], ['Y', 'Y', 'N'], ['Y', 'N', 'Y'], ['Y', 'N', 'N'], ['N', 'Y', 'Y'], ['N', 'Y', 'N'], ['N', 'N', 'Y'], ['N', 'N', 'N']]
    print(permutation_list_renamed) # [[], ['Y'], ['n'], ['Y', 'Y'], ['Y', 'n'], ['n', 'Y'], ['n', 'n'], ['Y', 'Y', 'Y'], ['Y', 'Y', 'n'], ['Y', 'n', 'Y'], ['Y', 'n', 'n'], ['n', 'Y', 'Y'], ['n', 'Y', 'n'], ['n', 'n', 'Y'], ['n', 'n', 'n']]

def rename_permutation_list(permutation_list, old, new):
    """ Renames all list elements 'old' in the lists in permutation_list to 'new' """
    permutation_list = copy.deepcopy(permutation_list)
    for permutation in permutation_list:
        for index, element in enumerate(permutation):
            if element == old:
                permutation[index] = new
    return permutation_list

def create_child(permutation,element):
    """ Returns a child of 'permutation' by shallow coping it and adding 'element'. """
    child = copy.copy(permutation)
    child.append(element)
    return child

def get_permutation_list(length, elements):
    """ Returns a list with all permutation lists of 'elements' up to length 'length'. """
    permutation_list = [[]]
    index = 0
    for i in range(length):
        end_index = len(permutation_list)
        for permutation in permutation_list[index:]:
            for element in elements:
                child = create_child(permutation,element)
                permutation_list.append(child)
        index = end_index
    return permutation_list

main()
