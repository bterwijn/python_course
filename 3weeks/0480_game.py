import pygame
import random

def main():
    game=Game()
    game.run()
    
class Ship:
    """ Ship that is controlled by the player """
    
    def __init__(self,position):
        """ Initalizes a Ship at given position """
        self.radius=20
        self.position=position

    def move(self,step):
        """ Moves Ship by step """
        speed=5
        self.position+=step*speed   
        
    def draw(self,window):
        """ Draws Ship in window """
        color=pygame.Color(255,255,255)
        pygame.draw.circle(window,color,self.position,self.radius)

    def is_in_collision(self,virus):
        """ Returns True if Ship is in collision with virus """
        return (self.position-virus.position).length()<self.radius+virus.radius
        
class Virus:
    """ A Virus ends the game by collising with the Ship """
    
    def __init__(self,position):
        """ Initalizes a Virus at given position """
        self.radius=10
        self.position=position
        self.speed=random.random()*5
        
    def move(self):
        """ Moves the Virus down """
        # speed=2
        self.position[1]+=self.speed
        self.position[0]+=(random.random()-0.5)*5

    def draw(self,window):
        """ Draws Virus in window """
        color=pygame.Color(255,0,0)
        pygame.draw.circle(window,color,self.position,self.radius)

class Game:

    def __init__(self):
        """ Initalizes the Game """
        pygame.init() # initialize pygame
        self.window = pygame.display.set_mode((800, 600)) # create drawing window
        size=pygame.Vector2(self.window.get_size())
        self.ship=Ship(size/2)
        self.viruses=[]
        self.running=True
        
    def run(self):
        """ Runs the Game until it ends """
        clock = pygame.time.Clock()
        while self.running:
            self.step()
            pygame.display.flip() # update the window
            clock.tick(60) # run at max 60 frames per second

    def step(self):
        """ Moves Game one step forward """
        for event in pygame.event.get(): # handle events
            if event.type == pygame.QUIT:
                self.running=False

        self.generate_viruses()
        self.move_viruses()
        self.handle_keyboard_events()
        if self.ship_is_in_collision():
            print("Game Over!")
            self.running=False
        self.draw_game()

    def handle_keyboard_events(self):
        """ Handles all the keyboard events to control the Ship """
        keys=pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.ship.move(pygame.Vector2(-1,0))
        if keys[pygame.K_RIGHT]:
            self.ship.move(pygame.Vector2(+1,0))
        if keys[pygame.K_DOWN]:
            self.ship.move(pygame.Vector2(0,1))
        if keys[pygame.K_UP]:
            self.ship.move(pygame.Vector2(0,-1))

    def generate_viruses(self):
        """ With a certain chance generate a new virus at a random position at the top of the window """
        new_virus_chance=0.05
        if random.random()<new_virus_chance:
            size=self.window.get_size()
            x=random.randint(0,size[0])
            y=0
            virus=Virus(pygame.Vector2(x,y))
            self.viruses.append(virus)
            
    def move_viruses(self):
        """ Maves all Virsuses """
        for virus in self.viruses:
            virus.move()

    def ship_is_in_collision(self):
        """ Check if ship is in  collision with any of the Virsuses """
        for virus in self.viruses:
            if self.ship.is_in_collision(virus):
                return True
        return False
        
    def draw_game(self):
        """ Draw all game elements """
        self.window.fill((0,0,0))
        self.ship.draw(self.window)
        for virus in self.viruses:
            virus.draw(self.window)

main()
