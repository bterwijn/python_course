
person=("James", "Taylor", 1.73, 65.5) # heterogeneous, immutable
print(len(person)) # 4
print(person) # ('James', 'Taylor', 1.73, 65.5)
    
line = ((0, 0), (1, 3))  # tuple of tuples
print(line)  # ((0, 0), (1, 3))
print(len(line))  # 2
(x1, y1), (x2, y2) = line
print("x1:", x1, "y1:", y1, "x2:", x2, "y2:", y2)

import math
def distance_to_origin(x, y):
    return math.sqrt(x*x+y*y)  # pythagoras

coordinate = (3, 4)
distance = distance_to_origin(*coordinate)  # unpack operator
print(distance)  # 5.0
