

def ask_user_for_int():
    return int(input(""))

def divide_two_ints():
    while True:
        try:
            print("Type int1: ",end='')
            int1=ask_user_for_int()
            break
        except ValueError:
            print("please try again for int1")
    print("Type int2: ",end='')
    int2=ask_user_for_int()
    return int1/int2

def main():
    while True:
        try:
            print(divide_two_ints())
            break
        except ValueError:
            print("please try again for int1 and int2")

main()
