import sys

def main():
    if len(sys.argv)<2:
        print_help()
        print(f"ERROR: Operator missing.")
        sys.exit(1)
    operator=sys.argv[1]
    arguments=sys.argv[2:]
    if operator=='-h' or operator=='--help':
        print_help()
    elif operator=='add':
        print(operate(lambda a,b: a+b,0,arguments))
    elif operator=='mult':
        print(operate(lambda a,b: a*b,1,arguments))
    else:
        print(f"ERROR: Invalid Operator '{operator}', either use 'add' or 'mult'.")
        sys.exit(1)
    sys.exit(0)

def print_help():
    print(f"Usage: python {sys.argv[0]} <Operator> [<float> ...]")
    print("Apply the Operator to all the float arguments and prints the result.")
    print("Operator can be 'add' for addition, or 'mult' for multiplication.")
    print(f"Example: $ python {sys.argv[0]} add 1 1.5 2")
    print(f"         $ 4.5")
    print()
    
def operate(operator,start_value,arguments):
    for argument in arguments:
        try:
            float_argument=float(argument)
        except ValueError:
            print(f"ERROR: Could not convert '{argument}' to float.")
            sys.exit(1)
        start_value=operator(start_value,float_argument)
    return start_value
    
main()
