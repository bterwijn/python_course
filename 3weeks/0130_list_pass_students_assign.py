'''Gegeven is een lijst van 'nr_students' studenten en een
corresponderende lijst van cijfers per student. Elke student heeft
'nr_grades' aantal cijfer waarbij elk cijfer gewogen moet worden met
een gegeven gewicht om het gemiddelde cijfer te bepalen. Schrijf
functie get_pass_students() die een lijst van studenten geeft die een
gemiddeld cijfer groter of gelijk aan de 'pass_grade' hebben.
'''

import random
import string

def main():
    random.seed(0)  # same random values each run
    nr_students = 4
    nr_grades = 3
    students = get_random_students(nr_students)
    grades = get_random_grades(nr_students, nr_grades)
    weights = get_random_weights(nr_grades)
    print("students:", students)
    print("grades:", grades)
    print("weight:", weights)
    pass_grade = 5.5
    print(get_pass_students(students, grades, weights, pass_grade)) # ['vtkgnkuhmp', 'xnhtqgxzvx', 'mwguoaskvr']


def get_random_name():
    """ Returns a random sequence on 10 lower case characters """ 
    name_length = 10
    return "".join(random.choices(string.ascii_lowercase, k=name_length))


def get_random_students(nr_students):
    """ Returns a list of 'nr_students' names """
    return [get_random_name() for s in range(nr_students)]


def get_random_grades(nr_students, nr_grades):
    """ Returns a list of 'nr_students' lists of 'nr_grades' grades between 
    1 and 10 rounded to 2 floating point digits. """
    return [[round(random.random() * 9 + 1, 2)
             for g in range(nr_grades)] for s in range(nr_students)]


def get_random_weights(nr_grades):
    """ Returns a list of 'nr_grades' weights between 0.5 and 2.5 rounded to 1 floating 
    point digit. """
    return [round(random.random() * 2 + 0.5, 1) for g in range(nr_grades)]


def get_pass_students(students, grades, weights, pass_grade):
    """Returns a list of students that have an average grade higher or
    equal to 'pass_grade'. The 'grades' contain the correspoding grades for
    each student that have to be weigted by the corresponding 'weights'."""
    pass_students = []
    for index, name in enumerate(students):
        grade = compute_grade(grades[index], weights)
        # print(name, grade)
        if grade >= pass_grade:
            pass_students.append(name)
    return pass_students
    return [name for index, name in enumerate(students)
            if compute_grade(grades[index], weights) >= pass_grade]  # or in just 1 line


def compute_grade(grades, weights):
    """ Computes the average of 'grades' weighted by the corresponding 'weights' rounded to 
    1 floating point digit. """
    sum = 0
    for index, value in enumerate(grades):
        sum += value * weights[index]
    return round(sum / len(grades),1)


main()
