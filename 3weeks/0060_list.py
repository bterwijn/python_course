
grades = [6, 7, 4, 8] # homogenous, mutable
print(type(grades))  # <class 'list'>

print(len(grades))  # 4
print(grades[0])    # 6
print(grades[1])    # 7
print(grades[-1])   # 8
print(grades[-2])   # 4
print(grades[-4])   # 6

# print(grades[ 5])  # IndexError: list index out of range
# print(grades[-5])  # IndexError: list index out of range

grades.append(9)        # mutable
print(grades)           # [6, 7, 4, 8, 9]
print(grades + [7, 5])  # [6, 7, 4, 8, 9, 7, 5]
print(grades)           # [6, 7, 4, 8, 9]
grades += [7, 5]        # grades = grades + [7, 5]
print(grades)           # [6, 7, 4, 8, 9, 7, 5]
print(grades*2)         # [6, 7, 4, 8, 9, 7, 5, 6, 7, 4, 8, 9, 7, 5]

grades.insert(1, 10)
print(grades)           # [6, 10, 7, 4, 8, 9, 7, 5]

grades.pop()
print(grades)           # [6, 10, 7, 4, 8, 9, 7]
grades.pop(1)
print(grades)           # [6, 7, 4, 8, 9, 7]
