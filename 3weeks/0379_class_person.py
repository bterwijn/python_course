
def main():
    person = {"name": "James", "surname": "Taylor", "weight": 70.5, "height": 1.71}
    print(type(person))                                         # <class 'dict'>
    print(person["name"], person["surname"], person["weight"])  # James Taylor 70.5

    person = Person("James", "Taylor", 70.5, 1.71)    # object of class Person
    print(type(person))                               # <class '__main__.Person'>
    print(person.name, person.surname, person.weight) # James Taylor 70.5
    
    print(get_full_name(person))                # James Taylor
    print("BMI:", get_body_mass_index(person))  # BMI: 23.938989774631512

class Person:

    def __init__(self, first_name, last_name, weight, height):
        self.name = first_name
        self.surname = last_name
        self.weight = weight
        self.height = height

def get_full_name(person):
    return person.name + ' ' + person.surname

def get_body_mass_index(person):
    return person.weight / person.height**2

main()
