
def main():
    person=Person("James", "Taylor", 70.5, 1.71)
    print(person.get_full_name())                # James Taylor
    print("BMI:", person.get_body_mass_index())  # BMI: 24.109982558736025
    print(person)  # name:James surname:Taylor weight:70.5 height:1.71

class Person:

    def __init__(self,first_name,last_name,weight,height):
        self.name=first_name
        self.surname=last_name
        self.weight=weight
        self.height=height

    def get_full_name(self): # method
        return self.name + ' ' + self.surname
        
    def get_body_mass_index(self):
        return self.weight / self.height**2

    def __repr__(self): # dunder/magic method
        return f"name:{self.name} surname:{self.surname} weight:{self.weight} height:{self.height}"
        
main()
