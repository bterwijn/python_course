
def add_score_to_score_list(*args, score_list=['all_scores:'],
                            sep=':', reverse=False):
    """
    Adds the 'args' scores of a match as a string to the score_list.

    Parameters:
        *args (int): number of goals of each team
        score_list (list): the list to add the score to
        sep (str): separator used in the score string
        reverse (bool): if true flips the order of team1 and team2

    Returns:
        The score_list with the new score added.
    """
    if reverse:
        args=args[::-1]
    score=""
    for i in args:
        if len(score)>0:
            score+=sep
        score+=str(i)
    score_list.append(score)
    return score_list

scores = add_score_to_score_list(1, 3, 4, 2, 5, score_list=[], reverse=True)
print(scores) # ['5:2:4:3:1']

help(add_score_to_score_list)
