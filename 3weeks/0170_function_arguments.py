
def add_score_to_score_list(team1_goals, team2_goals, score_list=
                            ['all_scores:'], sep=':', reverse=False):
    """
    Adds the score of a match as a string to the score_list.

    Parameters:
        team1_goals (int): number of goals of team1
        team2_goals (int): number of goals of team2
        score_list (list): the list to add the score to
        sep (str): separator used in the score string
        reverse (bool): if true flips the order of team1 and team2

    Returns:
        The score_list with the new score added.
    """
    if reverse:
        team1_goals, team2_goals = team2_goals, team1_goals
    score_list.append(str(team1_goals) + sep + str(team2_goals))
    return score_list

scores = add_score_to_score_list(1, 3)
print(scores)  # ['all_scores:', '1:3']
scores = add_score_to_score_list(1, 3, scores)
print(scores)  # ['all_scores:', '1:3', '1:3']
scores = add_score_to_score_list(1, 3, scores, reverse=True)
print(scores)  # ['all_scores:', '1:3', '1:3', '3:1']
scores = add_score_to_score_list(1, 3, scores, reverse=True, sep='-')
print(scores)  # ['all_scores:', '1:3', '1:3', '3:1', '3-1']

help(add_score_to_score_list)
