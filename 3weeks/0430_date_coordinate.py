import datetime as dt
import numpy as np

def main():
    now = dt.datetime.now()
    print(now)
    try:
        birthday = dt.datetime(1998, 5, 17)
    except ValueError:
        pass
    print(birthday)
    print(f"{seconds in between: (now-birthday).total_seconds()}")

    c1=np.array([1, 2])
    c2=np.array([3, 4])
    print(f"distance: {np.linalg.norm(c1-c2)}")
    
if __name__ == "__main__":
    main()
