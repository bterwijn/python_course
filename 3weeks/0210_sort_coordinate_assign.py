

def main():
    coordinates=[(3,4), (2,3), (5,4), (3,6)]
    sort_by_distance_to( (4,3), coordinates)
    print(coordinates) 

def sort_by_distance_to( coord, coordinates):
    coordinates.sort(key=lambda c : (coord[0]-c[0])**2 + (coord[1]-c[1])**2 )
    
main()
