import random
import math

def get_coordinate():
    max_x=1000
    max_y=2000
    return (random.random()*max_x,random.random()*max_y)

def distance_between_coordinates(c1,c2):
    """ Returns the distance between coordindate c1 and c2. """
    dx = c1[0] - c2[0]
    dy = c1[1] - c2[1]
    return math.sqrt(dx**2 + dy**2) # pythagoras

def main_slow(): # slow
    search_coordinate=(500,800)
    distance=10
    random.seed(0)
    nr_coordinates=100000
    coordinates=[get_coordinate() for i in range(nr_coordinates)]
    print(get_coordinates_within_distance_slow(search_coordinate,distance,coordinates))

def get_coordinates_within_distance_slow(search_coordinate,distance,coordinates):
    return [c for c in coordinates
            if distance_between_coordinates(search_coordinate,c)<distance]

def get_grid_cell(coordinate,distance):
    return (math.floor(coordinate[0]/distance),
            math.floor(coordinate[1]/distance))

def main(): # optimized using a dictionary
    search_coordinate=(500,800)
    distance=10
    random.seed(0)
    nr_coordinates=100000
    coordinates={}
    for i in range(nr_coordinates):
        coordinate=get_coordinate()
        grid_cell=get_grid_cell(coordinate,distance)
        if not grid_cell in coordinates:
            coordinates[grid_cell]=[]
        coordinates[grid_cell].append(coordinate)
        #print(coordinates)
    print(get_coordinates_within_distance(search_coordinate,distance,coordinates))

def get_coordinates_within_distance(search_coordinate,distance,coordinates):
    coordinates_within_distance=[]
    search_grid_cell=get_grid_cell(search_coordinate,distance)
    for x in range(-1,2):
        for y in range(-1,2):
            search_cell=(search_grid_cell[0]+x,search_grid_cell[1]+y)
            if search_cell in coordinates:
                for c in coordinates[search_cell]:
                    if distance_between_coordinates(search_coordinate,c)<distance:
                        coordinates_within_distance.append(c)
    return coordinates_within_distance

main()
