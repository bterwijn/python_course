
def main():
    persons=[]
    persons.append(Person("James", "Taylor",   70, 1.71))
    persons.append(Person("Jack",  "Smith",    67, 1.65))
    persons.append(Person("Emma",  "Williams", 65, 1.69))
    persons.sort(key=Person.get_body_mass_index)
    print(persons)

class Person:

    def __init__(self,first_name,last_name,weight,height):
        self.name=first_name
        self.surname=last_name
        self.weight=weight
        self.height=height

    def __repr__(self):
        return f"Person(name:{self.name} surname:{self.surname} weight:{self.weight} height:{self.height})"    
        
    def get_name(self):
        return f"{self.name} {self.surname}"
        
    def get_body_mass_index(self):
        return self.weight/self.height**2
    
if __name__ == "__main__":
    main()
