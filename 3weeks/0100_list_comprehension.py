n=3

my_list=[]
for i in range(n):
    my_list.append(i)
print(my_list) # [0, 1, 2]

my_list=list(range(n))
print(my_list) # [0, 1, 2]

my_list=[i for i in range(n)]
print(my_list) # [0, 1, 2]

square_surfaces=[i**2 for i in range(n)]
print(square_surfaces) # [0, 1, 4]

rectangle_surfaces=[width*height for width in range(n) for height in range(n)]
print(rectangle_surfaces) # [0, 0, 0, 0, 1, 2, 0, 2, 4]

rectangle_surfaces=[(width,height,width*height) for width in range(n) for height in range(n)]
print(rectangle_surfaces) # [(0, 0, 0), (0, 1, 0), (0, 2, 0), (1, 0, 0), (1, 1, 1), (1, 2, 2), (2, 0, 0), (2, 1, 2), (2, 2, 4)]

rectangle_surfaces=[(width,height,width*height) for width in range(n) for height in range(n) if height<width]
print(rectangle_surfaces) # [(1, 0, 0), (2, 0, 0), (2, 1, 2)]

rectangle_surfaces=[(width,height,width*height) for width in range(n) for height in range(width)]
print(rectangle_surfaces) # [(1, 0, 0), (2, 0, 0), (2, 1, 2)]
