

def main():
    people = ["Henk", "Jane", "Laura", "Bill", "Ana"]
    points = [46, 87, 59, 73, 61]
    top_3 = get_top_n(3, people, points)
    print(top_3)  # ['Jane', 'Bill', 'Laura']
    top_3 = get_top_n_alternative(3, people, points)
    print(top_3)  # ['Jane', 'Bill', 'Laura']

def get_top_n(n, people, points):
    indices = list(range(len(people)))
    indices.sort(key=lambda i: -points[i])
    return [people[i] for i in indices[:n]]

def get_top_n_alternative(n, people, points):
    combine = [(person, points[index]) for index, person in enumerate(people)]
    combine.sort(key=lambda person_points: person_points[1], reverse=True)
    return [i[0] for i in combine[:n]]

main()
