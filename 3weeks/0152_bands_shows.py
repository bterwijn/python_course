import copy

def main():
    bands = [ ['Ann', 'John'], ['Lisa', 'Paul', 'Judy'] ]
    shows = shedule_shows(bands) # schedule the bands for 4 shows, start with Ann,John and then alternate
    print('shows:',shows)
    # [['Ann', 'John'], ['Lisa', 'Paul', 'Judy'], ['Ann', 'John'], ['Lisa', 'Paul', 'Judy']]

    # 'Amy' joins the band of Ann and John for all scheduled and future shows
    bands[0].append('Amy')
    print('shows:',shows)
    # [['Ann', 'John', 'Amy'], ['Lisa', 'Paul', 'Judy'], ['Ann', 'John', 'Amy'], ['Lisa', 'Paul', 'Judy']]

    # 'Mark' will help Lisa,Paul,Judy as a guest performer in only the 4th show
    index=4-1
    shows[index]=copy.copy(shows[index])
    shows[index].append('Mark')
    print('shows:',shows)
    # [['Ann', 'John', 'Amy'], ['Lisa', 'Paul', 'Judy'], ['Ann', 'John', 'Amy'], ['Lisa', 'Paul', 'Judy', 'Mark']]

    # 'Lisa' changes her stage name to 'LiZa' for all scheduled and future shows
    bands[1][0]='LiZa'
    shows[index][0]='LiZa'
    print('shows:',shows)
    # [['Ann', 'John', 'Amy'], ['LiZa', 'Paul', 'Judy'], ['Ann', 'John', 'Amy'], ['LiZa', 'Paul', 'Judy', 'Mark']]
    
def shedule_shows(bands):
    shows=[]
    shows.append( bands[0] )
    shows.append( bands[1] )
    shows.append( bands[0] )
    shows.append( bands[1] )
    return shows
    
main()
