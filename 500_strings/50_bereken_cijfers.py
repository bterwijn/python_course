import sys

def gemiddelde_cijfer(cijfers):
    som=0
    for cijfer in cijfers:
        som+=float(cijfer)
    return som/len(cijfers)

def bereken_cijfer_voor_student(regel):
    split=regel.split(',')
    return split[0], gemiddelde_cijfer(split[1:])
    
def verwerk_bestand(bestandsnaam):
    with open(bestandsnaam, 'r') as bestand:
        for regel in bestand:
            print(*bereken_cijfer_voor_student(regel))
            
def main():
    if len(sys.argv)<2:
        print("usage: ",sys.argv[0]," <filename>")
    else:
        try:
            filename=sys.argv[1]
            verwerk_bestand(filename)
        except FileNotFoundError as e:
            print("File '",filename,"' does not exist.")

main()
