import sys

separator="|"

def load_log(log_filename):
    data=[]
    try:
        with open(log_filename) as file:
            for line in file:
                data=line.split(separator)
    except FileNotFoundError as e:
        pass
    return data

def save_log(data,log_filename):
    try:
        with open(log_filename, "w") as file:
            first=True
            for value in data:
                if not first:
                    file.write(separator)
                file.write(value)
                first=False
    except FileNotFoundError as e:
        print("could not write to:"+log_filename)
        
def update_log(log_filename,command,value):
    data=load_log(log_filename)
    if command=="set":
        data.append(value)
        save_log(data,log_filename)
    elif command=="get":
        if len(data)>0:
            print(data[-1])
    elif command=="print":
        for i in data:
            print(i)
    elif command=="remove_last":
        data.pop()
        save_log(data,log_filename)
    elif command=="remove_all":
        data=[]
        save_log(data,log_filename)
    elif command=="add":
        if len(data)>0:
            try:
                last_value=float(data[-1])
                add_value=float(value)
                data.append(str(last_value+add_value))
                save_log(data,log_filename)
            except ValueError as e:
                print("not a number")
    else:
        print("unknown command:",command)
        
def main():
    if len(sys.argv)<3:
        print("usage: ",sys.argv[0]," <log-filename> <command> [value]")
    else:
        log_filename=sys.argv[1]
        command=sys.argv[2]
        value= sys.argv[3] if len(sys.argv)>=4 else ""
        update_log(log_filename,command,value)
        
main()
