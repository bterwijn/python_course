import sys

def encrypt_file(flag,password,input_filename,output_filename):
    try:
        infile=open(input_filename,"rb")
    except FileNotFoundError as e:
        print("could read from file:",input_filename)
        exit(1)
    try:
        outfile=open(output_filename,"wb")
    except FileNotFoundError as e:
        print("could not write to file:",output_filename)  
        exit(1)
    password=password.encode()
    while True:
        block=bytearray(infile.read(len(password)))
        if len(block)==0:
            break
        for index in range(len(block)):
            if flag=="-e":
                block[index]=(block[index]+password[index])%256
            elif flag=="-d":
                block[index]=(block[index]-password[index])%256
        outfile.write(block)
            
def main():
    if len(sys.argv)<4:
        print("usage: ",sys.argv[0]," <flag> <password> <input-filename> <output-filename>")
        print("  flags:")
        print("   -e  encrypt the input file using password")
        print("   -d  decrypt the input file using password")
    else:
        flag=sys.argv[1]
        password=sys.argv[2]
        input_filename=sys.argv[3]
        output_filename=sys.argv[4]
        encrypt_file(flag,password,input_filename,output_filename)
        
main()
