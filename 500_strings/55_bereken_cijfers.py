import sys

def gemiddelde_cijfer(cijfers):
    som=0
    for cijfer in cijfers:
        som+=float(cijfer)
    return som/len(cijfers)

def bereken_cijfer_voor_student(regel):
    split=regel.split(',')
    return split[0], gemiddelde_cijfer(split[1:])
    
def verwerk_bestand(input_filename,output_filename):
    out_file = sys.stdout if output_filename is None else open(output_filename, "w")        
    with open(input_filename, 'r') as in_file:
        for regel in in_file:
            print(*bereken_cijfer_voor_student(regel),file=out_file)
    out_file.close()
                
def main():
    if len(sys.argv)<2:
        print("usage: ",sys.argv[0]," <input-file> [<output-file>]")
    else:
        try:
            input_filename=sys.argv[1]
            output_filename=output_filename=sys.argv[2] if len(sys.argv)>2 else None
            verwerk_bestand(input_filename,output_filename)
        except FileNotFoundError as e:
            print("File '",filename,"' does not exist.")

main()
