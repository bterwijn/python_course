import sys

def load_file(filename):
    with open(filename) as file:
        data=file.read()
    return data

def save_file(data,filename):
    with open(filename, "w") as file:
        file.write(data)

def find_replace(filename,old,new):
    try:
        data=load_file(filename)
        data=data.replace(old,new)
        save_file(data,filename)
    except FileNotFoundError as e:
        print("could not find file:",filename)
        
def main():
    if len(sys.argv)<4:
        print("usage: ",sys.argv[0]," <filename> <old> <new>")
    else:
        filename=sys.argv[1]
        old=sys.argv[2]
        new=sys.argv[3]
        find_replace(filename,old,new)
        
main()
