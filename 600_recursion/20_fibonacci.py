

def multiply_iterative(a,b):
    if a<0:
        a=-a
        b=-b
    result=0
    for i in range(a):
        result+=b
    return result

def multiply_recursive(a,b):
    if a<0:
        a=-a
        b=-b
    if a==0:
        return 0
    else:
        return b+multiply_recursive(a-1,b)
    
def main():
    a=-3
    b=-4
    print("multiply_iterative(",a,",",b,")=", multiply_iterative(a,b) )
    print("multiply_recursive(",a,",",b,")=", multiply_recursive(a,b) )
    
main()
