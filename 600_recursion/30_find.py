import random

def random_values(n,max_value):
    values=[]
    for i in range(n):
        values.append( random.randint(0,max_value) )
    return values

def find_iterative(value,values):
    low=0
    high=len(values)
    while True:
        mid=(low+high)//2
        if values[mid]==value:
            return True
        if low==mid:
            return False
        elif values[mid]<value:
            low=mid
        elif values[mid]>value:
            high=mid

def find_recursive_helper(value,values,low,high):
    mid=(low+high)//2
    if values[mid]==value:
        return True
    if low==mid:
        return False
    elif values[mid]<value:
        low=mid
    elif values[mid]>value:
        high=mid
    return find_recursive_helper(value,values,low,high)
    
def find_recursive(value,values):
    return find_recursive_helper(value,values,0,len(values))

def main():
    n=100
    max_value=200
    value=random.randint(0,max_value)
    values=random_values(n,max_value)
    values.sort()
    print("searching value",value,"in:",values)
    print("find_iterative(",value,")=", find_iterative(value,values) )
    print("find_recursive(",value,")=", find_recursive(value,values) )
    
main()
