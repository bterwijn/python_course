
def print_paths(path, destination, edges):
    current=path[-1]
    if current==destination:
        print("path:",path)
    else:
        for e1,e2 in edges:
            if e1==current and not e2 in path:
                path.append(e2)
                print_paths(path, destination, edges)
                path.pop()
            if e2==current and not e1 in path:
                path.append(e1)
                print_paths(path, destination, edges)
                path.pop()

def main():
    print_paths(['A'],'E',[ ('A','B'), ('A','D'), ('B','C'), ('B','D'), ('C','E'), ('C','F'), ('C','D'), ('D','F'), ('E','F') ])

main()
