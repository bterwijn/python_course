import copy

def permutations_helper(n,values,permutation,result):
    if n<=0:
        result.append(copy.deepcopy(permutation))
    else:
        for i in values:
            permutation.append(i)
            permutations_helper(n-1,values,permutation,result)
            permutation.pop()
    
def permutations(n,values):
    result=[]
    permutations_helper(n,values,[],result)
    return result

def main():
    n=3
    print( permutations(n,['a','b']) )

main()
