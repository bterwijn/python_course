import copy

def permutations_helper(n,values,permutation):
    if n<=0:
        print(permutation)
    else:
        for i in values:
            permutation.append(i)
            permutations_helper(n-1,values,permutation)
            permutation.pop()
    
def permutations(n,values):
    permutations_helper(n,values,[])

def main():
    n=3
    permutations(n,['a','b'])

main()
