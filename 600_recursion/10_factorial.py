

def factorial_iterative(n):
    result=1
    for i in range(1,n+1):
        result*=i
    return result

def factorial_recursive(n):
    if n<=0:
        return 1
    else:
        return n*factorial_recursive(n-1)
    
def main():
    n=3
    print("factorial_iterative(",n,")=", factorial_iterative(n) )
    print("factorial_recursive(",n,")=", factorial_recursive(n) )
    
main()
