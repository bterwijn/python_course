import pygame
import random
import math

def draw_tree(pygame,screen,step,x,y,length,angle,color,thick):
    if step<13:
        x2=x+math.cos(angle)*length
        y2=y-math.sin(angle)*length
        delta_angle=math.pi/5.5
        length2=length*0.76
        thick2=thick*0.8
        color_max_step=20
        color2=pygame.Color((round(color[0]*0.8))%256,
                            (round(color[1]    ))%256,
                            (round(color[2]*0.8))%256)
        pygame.draw.line(screen,color,(x,y),(x2,y2),round(thick))
        draw_tree(pygame,screen,step+1,x2,y2,length2,angle+delta_angle,color2,thick2)
        draw_tree(pygame,screen,step+1,x2,y2,length2,angle-delta_angle,color2,thick2)
    
def main():
    pygame.init()
    screen = pygame.display.set_mode((800, 600))

    display_size = pygame.display.get_surface().get_rect()
    color=pygame.Color(255,255,255)
    draw_tree(pygame,screen,0,display_size[2]/2,display_size[3],150,math.pi/2,color,20)
    
    pygame.display.flip()

    running=True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running=False

    pygame.quit()

main()

