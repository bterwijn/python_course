import copy

class Coordinate:

    def __init__(self,x,y):
        self.x=x
        self.y=y

    def __repr__(self):
        return "({},{})".format(
            self.x,self.y)
        
    def add(self,coord2):
        self.x+=coord2.x
        self.y+=coord2.y

def add(coord1,coord2):
    new_coord=copy.deepcopy(coord1)
    new_coord.add(coord2)
    return new_coord

def main():
    c1=Coordinate(1,2)
    c2=Coordinate(3,4)
    print( "c1:",c1,"c2:",c2 )
    print( add(c1,c2) )
    print( "c1:",c1,"c2:",c2 )
    c1.add(c2)
    print( "c1:",c1,"c2:",c2 )
    
main()

