

class Result:
    def __init__(self,team_name1,goals1,goals2,team_name2):
        self.team_name1=team_name1
        self.goals1=goals1
        self.goals2=goals2
        self.team_name2=team_name2
                
    def __repr__(self):
        return "{} {}:{} {}".format(self.team_name1,self.goals1,self.goals2,self.team_name2)

    def has_played(self,team_name):
        return team_name==team_name1 or team_name==team_name2
    
    def get_goals_scored(self,team_name):
        if team_name==team_name1:
            return self.goals1
        elif team_name==team_name2:
            return self.goals2
        return 0

    def get_goals_conceded(self,team_name):
        if team_name==team_name1:
            return self.goals2
        elif team_name==team_name2:
            return self.goals1
        return 0

class Team:
    win_points=3
    loose_points=0
    draw_points=1
    
    def __init__(self,name):
        self.name=name
        self.win_count=0
        self.loose_count=0
        self.draw_count=0
        self.points=0
        self.goals_scored=0
        self.goals_conceded=0

    def __repr__(self):
        return "{} {} {} {}".format(self.name,self.points,self.goals2,self.team_name2)

    def get_match_count(self):
        return self.win_count+self.loose_count+self.draw_count

    def get_points(self):
        return self.win_count*win_points+self.loose_count*loose_points+self.draw_count*draw_points
    
    def update(result):
        if result.has_played(self.name):
            goals_scored=result.get_goals_scored(self.name)
            goals_conceded=result.get_goals_conceded(self.name)
            self.goals_scored+=goals_scored
            self.goals_conceded+=goals_conceded
            if goals_scored>goals_conceded:
                self.win_count+=1
            elif goals_scored<goals_conceded:
                self.loose_count+=1
            else:
                self.draw_count+=1
        
class Ranking:
    def __init__(self):
        self.teams=[]

    def add_result(result):
        for i in self.teams:
            pass

    
