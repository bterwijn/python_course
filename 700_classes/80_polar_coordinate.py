import math
import copy

# ---------------------------------- import!
class Coordinate:

    def __init__(self,x,y):
        self.x=x
        self.y=y

    def __repr__(self):
        return "({},{})".format(
            self.x,self.y)
        
    def add(self,coord2):
        self.x+=coord2.x
        self.y+=coord2.y

def add(coord1,coord2):
    new_coord=copy.deepcopy(coord1)
    new_coord.add(coord2)
    return new_coord

# ---------------------------------- import!
def cartesian_naar_polar(x,y):
    return math.atan2(y,x), math.sqrt(x**2+y**2)

def polar_naar_cartesian(angle,distance):
    return math.cos(angle)*distance, math.sin(angle)*distance
# ----------------------------------

def polar_to_cartesian(coord):
    return Coordinate( *polar_naar_cartesian(coord.angle,coord.distance) )
    
def cartesian_to_polar(coord):
    return Polar_Coordinate( *cartesian_naar_polar(coord.x,coord.y) )
    
class Polar_Coordinate:

    def __init__(self,angle,distance):
        self.angle=angle
        self.distance=distance

    def __repr__(self):
        return "{}:{}".format(
            self.angle,self.distance)
    
    def add(self,coord2):
        c1=polar_to_cartesian(self)
        c2=polar_to_cartesian(coord2)
        c1.add(c2)   
        p1=cartesian_to_polar(c1)
        self.angle=p1.angle
        self.distance=p1.distance

def main():
    c1=Polar_Coordinate(0,2)
    c2=Polar_Coordinate(math.pi,1)
    print( "c1:",c1,"c2:",c2 )
    print( add(c1,c2) )
    print( "c1:",c1,"c2:",c2 )
    c1.add(c2)
    print( "c1:",c1,"c2:",c2 )
    
main()

