

class Person:

    def __init__(self,first_name,last_name,age,weight,height):
        self.name=first_name
        self.surname=last_name
        self.age=age
        self.weight=weight
        self.height=height

    def get_name(self):
        return self.name+" "+self.surname
        
    def get_body_mass_index(self):
        return self.weight/self.height**2

    def __str__(self):
         return "name:{} surname:{} age:{} weight:{} height:{}".format(
             self.name,self.surname,self.age,self.weight,self.height)

def main():

    person=Person("James", "Bond", 85, 70, 1.71)
    print(type(person))
    print(person.get_name())
    print("BMI:", person.get_body_mass_index())
    print( person )
    
main()
