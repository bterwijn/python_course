import copy

class Grade:
    def __init__(self,value,weight):
        self.value=value
        self.weight=weight

    def __repr__(self):
        return "value:{} weight:{}".format(
             self.value,self.weight)

    def add(self,grade2):
        self.value+=grade2.value
        self.weight+=grade2.weight

    def subtract(self,grade2):
        self.value-=grade2.value
        self.weight-=grade2.weight

    def get_average(self):
        return self.value/self.weight
        
class Grades:
    def __init__(self):
        self._grades=[]
        self._total_grade=Grade(0,0)

    def __repr__(self):
        return "_grades:{} _total_grade:{}".format(
             self._grades,self._total_grade)
        
    def add_grade(self,grade,index=None):
        if index==None:
            self._grades.append(grade)
        else:
            self._grades.insert(index,grade)
        self._total_grade.add(grade)

    def get_average_grade(self):
        return self._total_grade.get_average()
        
    def get_nr_of_grades(self):
        return len(self._grades)
    
    def remove_grade(self,index):
        self._total_grade.subtract(self._grades[index])
        return self._grades.pop(index)
        
class Student:
    minimal_pass_grade=5.5
    
    def __init__(self):
        self.grades=Grades()

    def __repr__(self):
        return "grades:{}".format(
             self.grades)
        
    def add_grade(self,grade):
        self.grades.add_grade(grade)

    def get_average_grade(self):
        return self.grades.get_average_grade()

    def is_passed(self):
        return self.grades.get_average_grade()>=Student.minimal_pass_grade

    def remove_one_old_grade_to_pass(self):
        if self.is_passed():
                return True
        for index in range(self.grades.get_nr_of_grades()):
            remove_grade=self.grades.remove_grade(index)
            print(self)
            if self.is_passed():
                return True
            self.grades.add_grade(remove_grade,index)
        return False

def main():
    student=Student()
    student.add_grade( Grade(6,1) )
    student.add_grade( Grade(8,1) )
    student.add_grade( Grade(1,2) )
    print(student)
    print(student.get_average_grade())

    student.remove_one_old_grade_to_pass()
    print(student)
    print(student.get_average_grade())
    
main()
