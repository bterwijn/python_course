
class Person:
    _counter=0
    
    def __init__(self,first_name,last_name,weight,height):
        self.name=first_name
        self.surname=last_name
        self.weight=weight
        self.height=height
        Person._counter+=1
        self.id_number=Person._counter
        
    def get_counter():
        return Person._counter
    
    def __repr__(self):
         return "name:{} surname:{} weight:{} height:{} id_number:{}\n".format(
             self.name,self.surname,self.weight,self.height,self.id_number)

def main():

    persons=[]
    person= Person("James", "Bond", 70, 1.71)
    print( person )
    persons.append( person )
    
    persons.append( Person("David", "Taylor", 23, 1.78) )
    persons.append( Person("John", "Johnson", 60, 1.60) )
    persons.append( Person("Robert", "Smith", 55, 1.65) )
    print(persons)

    print("Person counter:", Person.get_counter() )
    print("length of persons list:", len(persons) )
    
main()
