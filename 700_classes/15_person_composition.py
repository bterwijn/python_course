
class Date:

    def __init__(self,day,month,year):
        self._year=year
        self._month=month
        self._day=day
        if not self.is_valid():
            raise ValueError('Invalid Date: '+str(self))

    def is_leap_year(self):
        leap_year=False
        if self._year%4:            # every fourth year is a leap year, except when
            if self._year%100:      # it is devisible by 100, with the exception
                if self._year%400:  # for years devisible by 400
                    leap_year=True
            else:
                leap_year=True
        return leap_year
        
    def is_valid(self):
        if self._month<1 or self._month>12:
            return False
        if self._day<1 or self._day>31:
            return False
        if self._month in {4,6,9,11} and self._day>30:
            return False        
        if self._month==2:
            if self.is_leap_year():
                if self._month>29:
                    return False
            else:
                if self._month>28:
                    return False
        return True
        
    def year_difference(self,date2):
        result=date2._year-self._year
        if date2._month<self._month:
            result-=1
        elif date2._month==self._month and date2._day<self._day:
            result-=1
        return result

    def __str__(self):
        return "{}-{}-{}".format(self._day,self._month,self._year)

class Person:

    def __init__(self,first_name,last_name,birthdate,weight,height):
        self.name=first_name
        self.surname=last_name
        self.birthdate=birthdate
        self.weight=weight
        self.height=height

    def get_name(self):
        return self.name+" "+self.surname
        
    def get_body_mass_index(self):
        return self.weight/self.height**2

    def years_old(self,date2):
        return self.birthdate.year_difference(date2)
        
    def __str__(self):
         return "name:{} surname:{} date-of-birth:{} weight:{} height:{}".format(
             self.name,self.surname,self.birthdate,self.weight,self.height)

def main():

    birthdate=Date(11,11,1920)
    person=Person("James", "Bond", birthdate, 70, 1.71)
    print( person )
    
    today=Date(1,1,2000)
    print("age at",today,":",person.years_old(today))
    print("age at",today,":",person.years_old( Date(1,1,2000) ))
    
main()
