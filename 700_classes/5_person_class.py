

class Person:
    def __init__(self,first_name,last_name,birthdate,weight,height):
        self.name=first_name
        self.surname=last_name
        self.birthdate=birthdate
        self.weight=weight
        self.height=height

def print_name(person):
    print(person.name+" "+person.surname)

def get_body_mass_index(person):
    return person.weight/person.height**2
    
def main():
    person=("James", "Bond", 85, 70, 1.71)  # tuple
    print(person[0], person[1])
    
    person=Person("James", "Bond", 85, 70, 1.71) # object
    print_name(person)
    print( get_body_mass_index(person) )
    
main()
