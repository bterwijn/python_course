'''Schatgraven

Je hebt een schat begraven op punt (100,130) en je wilt een schatkaart
maken met instructie om de schat weer terug te kunnen vinden vanaf
punt (50,50). De instructie moet bestaan uit een richting in graden en
een afstand lopen. Welke richting en afstand schrijf je op de
schatkaart?

'''
import math

schat_x=100
schat_y=130
start_x=50
start_y=50
verschil_x=schat_x-start_x
verschil_y=schat_y-start_y

radialen=math.atan2(verschil_y,verschil_x)
graden=radialen*180/math.pi
afstand=math.sqrt(verschil_x**2+verschil_y**2)

print("De schat ligt op",afstand,"meter lopen in de richting van",graden,"graden")
