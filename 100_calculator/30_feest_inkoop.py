'''Feest inkoop

Je hebt 100 liter champagne beschikbaar om te schenken op een
feest. Elke gast wil je 0.45 liter champagne schenken. Daarnaast wil
je elke gast 0.3 kg oesters serveren. Je koopt oesters in verpakkingen
van 2 kilogram voor 36.50 euro. Voor hoeveel euros moet je aan oesters
inkopen als je het maximaal aantal gasten (op basis van beschikbare
champagne) uitnodigd?

'''
import math

champagne=100
champagne_per_gast=0.45
gasten=champagne//champagne_per_gast
oesters_per_gast=0.3
oesters=gasten*oesters_per_gast
verpakkingen_kg=2
verpakkingen_kost=36.50
verpakkingen=math.ceil(oesters/verpakkingen_kg)

print("Kosten voor de oester:",verpakkingen*verpakkingen_kost)
