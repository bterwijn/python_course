
'''Auto Onderhoud 

Voor elke auto die binnenkomt moet je 4 wielen vervangen. Je koopt de
wielen in sets van 25. Dus als 10 auto's binnenkomen moet je 2 sets
inkopen en hou je 15 wielen over.

Er komen nu 253 auto's binnen. Hoeveel set moet je inkopen en hoeveel wielen hou je over?
'''

autos=253
wielen=autos*4
sets=wielen//25
wielen_over=wielen%25
print("sets:",sets,"wielen_over:",wielen_over)
