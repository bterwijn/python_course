'''Schatgraven

De schatkaart zegt dat de schat begraven ligt op:

  35 graden 100 meter lopen
  120 graden 50 meter lopen
  80 graden 30 meter lopen

vanaf punt beginpunt (100,50). Wat is de x en y coordinaat van het
punt waar de schat begraven ligt?

'''
import math

x=100
y=50

radians = 35 *180/math.pi
x+= math.cos( radians ) * 100
y+= math.sin( radians ) * 100

radians = 120 *180/math.pi
x+= math.cos( radians ) * 50
y+= math.sin( radians ) * 50

radians = 80 *180/math.pi
x+= math.cos( radians ) * 30
y+= math.sin( radians ) * 30

print("De schat ligt op punt x:",x,"y:",y)
