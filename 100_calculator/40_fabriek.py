'''Fabriek

Een Fabriek maakt een stoel van 4.5 kg hout, 1.5 kg leer, en 0.4 kg
staal. De fabriek heeft nog 404 kg hout, 136 kg leer, en 38 kg staal
op voorraad. Het duurt 2.23 uur om een stoel te maken. Een werkdag
duurt 8 uur. Hoeveel werkdagen zijn er nodig om de voorraad op te
maken?

'''
import math

nodig_hout = 4.5
nodig_leer = 1.5
nodig_staal = 0.4
voorraad_hout = 404
voorraad_leer = 136
voorraad_staal = 38
stoel = min( voorraad_hout//nodig_hout, voorraad_leer//nodig_leer, voorraad_staal//nodig_staal )
stoel_tijd = 2.23
werktijd =  stoel*stoel_tijd
werkdag_tijd = 8
werkdagen = math.ceil( werktijd/werkdag_tijd )

print("Werkdagen om voorraad op te maken:",werkdagen)
