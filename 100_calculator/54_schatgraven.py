'''Schatgraven

Je hebt een schat begraven op punt (50,80) en je wilt een schatkaart
maken met instructie om de schat weer terug te kunnen vinden vanaf
punt (0,0). De instructie moet bestaan uit een richting in radialen en
een afstand lopen. Welke richting en afstand schrijf je op de
schatkaart?

'''
import math

schat_x=50
schat_y=80

radialen=math.atan2(schat_y,schat_x)
afstand=math.sqrt(schat_x**2+schat_y**2)

print("De schat ligt op",afstand,"meter lopen in de richting van",radialen,"radialen")
