'''Schatgraven

De schatkaart zegt dat de schat begraven ligt op:

  0.6 radialen 100 meter lopen

vanaf punt beginpunt (0,0). Wat is de x en y coordinaat van het punt
waar de schat begraven ligt?

'''
import math

radians = 35 *180/math.pi
x= math.cos( 0.6 ) * 100
y= math.sin( 0.6 ) * 100

print("De schat ligt op punt x:",x,"y:",y)
