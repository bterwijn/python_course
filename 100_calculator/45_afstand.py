'''Afstand

Bereken de afstand tussen punt (2,4,3) en (6,5,2).
'''
import math

x1=2
y1=4
z1=3

x2=6
y2=5
z2=2

afstand= math.sqrt( (x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2 )

print("De afstand is:",afstand)
