
'''Maanden

Een jaar telt 12 maanden:

  1:jan 2:feb 3:mar 4:apr 5:mei 6:jun 7:jul 8:aug 9:set 10:oct 11:nov 12:dec 

Stel de huidige maand is maand-nummer 5 (mei). 6 maanden later is het
dan maand-nummer 11 (nov).

Welk maand-nummer is het 9 maanden later?
Welk maand-nummer is het 19 maanden later?
Welk maand-nummer is het 5234 maanden later?
'''

maanden=12
maand=5
maand_zero=maand-1 # use instead   0:(jan) 1:(feb) ... 11:(dec)
print("9 maanden later is het maand-nummer:", 1+ (maand_zero+9) % maanden )
print("19 maanden later is het maand-nummer:", 1+ (maand_zero+19) % maanden )
print("5234 maanden later is het maand-nummer:", 1+ (maand_zero+5234) % maanden )
