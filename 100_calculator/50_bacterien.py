'''Bacterien

Een bacterien-populatie verdubbelt elke 2.1 uur. We starten met een
populatie van 1000 bacterien. 

Hoeveel bacterien hebben we na 24 uur?
Hoeveel uur duurt het voordat we 2 miljard bacterien hebben?

Beide vragen hoeven niet te worden afgerond naar hele bacterien of uren.
'''
import math

bacterien = 1000
verdubbel_tijd = 2.1

na_24_uur = bacterien * math.pow(2, 24 / verdubbel_tijd)
print("na 24 uur hebben we",na_24_uur,"bacterien")

verdubbelingen_2_miljard = math.log( 2*(10**9) / bacterien , 2)  # aantal verdubbelingen nodig om tot 2 miljard te komen
tijd_2_miljard = verdubbelingen_2_miljard*verdubbel_tijd
print("het duurt",tijd_2_miljard,"uur voordat we 2 miljard bacterien hebben")
