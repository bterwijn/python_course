import pygame
import random

pygame.init()
screen = pygame.display.set_mode((800, 600))

clock = pygame.time.Clock()
running=True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False
    
    display_size = pygame.display.get_surface().get_rect()
    x1=random.randint(display_size[0],display_size[2])
    y1=random.randint(display_size[1],display_size[3])
    x2=random.randint(display_size[0],display_size[2])
    y2=random.randint(display_size[1],display_size[3])
    max_color=255
    color=pygame.Color(random.randint(0,max_color),
                       random.randint(0,max_color),
                       random.randint(0,max_color))
    pygame.draw.line(screen,color,(x1,y1),(x2,y2),4)

    pygame.display.flip()
    clock.tick(60)

pygame.quit()
