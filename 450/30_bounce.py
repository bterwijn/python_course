import pygame

pygame.init()
screen = pygame.display.set_mode((800, 600))

color=pygame.Color(255,0,0)
pos=pygame.Vector2(400,400)
speed=pygame.Vector2(2,-3)
radius=40
width=8

clock = pygame.time.Clock()
running=True
while running:
    screen.fill((0,0,0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False

    keys=pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        speed[0]-=1
    if keys[pygame.K_RIGHT]:
        speed[0]+=1
    if keys[pygame.K_DOWN]:
        speed[1]+=1
    if keys[pygame.K_UP]:
        speed[1]-=1
    speed[1]+=0.5
    speed*=0.99
    pos+=speed
    
    display_size = pygame.display.get_surface().get_rect()
    if pos[0]<display_size[0] or pos[0]>display_size[2]:
        pos-=speed
        speed[0]=-speed[0]
    if pos[1]<display_size[1] or pos[1]>display_size[3]:
        pos-=speed
        speed[1]=-speed[1]

    pygame.draw.circle(screen, color, pos, radius, width)
    pygame.display.flip()
    clock.tick(60)

pygame.quit()
