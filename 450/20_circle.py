import pygame

pygame.init()
screen = pygame.display.set_mode((800, 600))

color=pygame.Color(255,0,0)
pos=pygame.Vector2(400,400)
radius=40
width=8
speed=10

clock = pygame.time.Clock()
running=True
while running:
    screen.fill((0,0,0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False

    keys=pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        pos[0]-=speed
    if keys[pygame.K_RIGHT]:
        pos[0]+=speed
    if keys[pygame.K_DOWN]:
        pos[1]+=speed
    if keys[pygame.K_UP]:
        pos[1]-=speed

    display_size = pygame.display.get_surface().get_rect()

    if pos[0]<display_size[0]:
        pos[0]=display_size[0]
    if pos[0]>display_size[2]:
        pos[0]=display_size[2]
    if pos[1]<display_size[1]:
        pos[1]=display_size[1]
    if pos[1]>display_size[3]:
        pos[1]=display_size[3]

    pygame.draw.circle(screen, color, pos, radius, width)
    pygame.display.flip()
    clock.tick(60)

pygame.quit()
