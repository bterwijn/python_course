import pygame
import math

pygame.init()
screen = pygame.display.set_mode((800, 600))

color=pygame.Color(255,0,0)
pos=pygame.Vector2(400,400)
direction=0
speed=pygame.Vector2(0,0)
accel=0.3
radius=40
width=8

def polar_to_cartesian(angle,distance):
    return pygame.Vector2(math.cos(angle),
                          math.sin(angle)) * distance

clock = pygame.time.Clock()
running=True
while running:
    screen.fill((0,0,0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False

    keys=pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        direction-=0.1
    if keys[pygame.K_RIGHT]:
        direction+=0.1
    if keys[pygame.K_DOWN]:
        speed-=polar_to_cartesian(direction,accel)
    if keys[pygame.K_UP]:
        speed+=polar_to_cartesian(direction,accel)
    speed*=0.99
    pos+=speed
    
    display_size = pygame.display.get_surface().get_rect()
    if pos[0]<display_size[0] or pos[0]>display_size[2]:
        pos-=speed
        speed[0]=-speed[0]
    if pos[1]<display_size[1] or pos[1]>display_size[3]:
        pos-=speed
        speed[1]=-speed[1]

    pygame.draw.circle(screen, color, pos, radius, width)
    pos2=pos+polar_to_cartesian(direction,radius*1.5)

    # pygame.draw.line(screen, color, pos, pos2, width)
    # pygame.draw.circle(screen, color, pos, width/2, width)
    # pygame.draw.circle(screen, color, pos2, width/2, width)

    d=polar_to_cartesian(direction+math.pi/2,width/2)
    pygame.draw.polygon(screen, color, [ pos+d,pos2+d,pos2-d,pos-d ])
    

    
    pygame.display.flip()
    clock.tick(60)

pygame.quit()
