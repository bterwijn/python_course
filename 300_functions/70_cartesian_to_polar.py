'''Cartesian naar en van Polar

Schrijf de functie cartesian_naar_polar(x,y) die de x en y coordinaat
van een punt omzet naar angle (in radians) en distance:

  angle,distance = cartesian_naar_polar(x,y)

En schrijf de functie polar_naar_cartesian(angle,distance) die het
omgekeerde doet:

  x,y = polar_naar_cartesian(angle,distance)

Vraag vervolgens de gebruiker om een Cartesian punt. Zet de Cartesian
representatie om naar een Polar representatie van dat punt. Zet daarna
de Polar representatie dan weer terug om naar Cartesian. Print de
steeds de resultaten.

'''
import math

def cartesian_naar_polar(x,y):
    return math.atan2(y,x), math.sqrt(x**2+y**2)

def polar_naar_cartesian(angle,distance):
    return math.cos(angle)*distance, math.sin(angle)*distance

def vraag_cartesian_punt():
    print("Geef een 2D Cartesian punt:")
    x = float(input("x:"))
    y = float(input("y:"))
    return x,y

def main():
    x,y=vraag_cartesian_punt()
    print("Cartesian punt:",x,y)
    angle,distance = cartesian_naar_polar(x,y)
    print("Polar punt:",angle,distance)
    print("Cartesian punt:", *polar_naar_cartesian(angle,distance) )
    
main()
