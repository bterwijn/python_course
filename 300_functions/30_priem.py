'''Priem

Een priemgetal is een positief geheel getal dat alleen deelbaar is
door zichzelf en door 1 en niet door andere gehele getallen. 

Bijvoorbeeld:
 2 is een priemgetal
 3 is een priemgetal
 4 is geen priemgetal want 2x2=4
 5 is een priemgetal
 6 is geen priemgetal want 2x3=6
 ...

Vraag de gebruiker tussen welk begin- en eindgetal (inclusief) hij/zij
priemgetallen wil zoeken en geef dan het totale aantal priemgetallen
wat hier tussen ligt.

'''
import math

def vraag_begin_eind():
    print("Tussen welke getallen (inclusief) wil je priemgetallen zoeken?")
    begin=int(input("begin: "))
    eind=int(input("eind: "))
    return begin, eind

def priem(n):
    max_deler=math.floor( math.sqrt(n) )
    for i in range(2,max_deler+1):
        if n%max_deler==0:
            return False
    return True

def tel_priemgetallen(begin,eind):
    teller=0
    for i in range(begin,eind+1):
        if priem(i):
            teller+=1
    return teller

def main(): 
    print("Aantal gevonden priemgetallen:", tel_priemgetallen( *vraag_begin_eind() ) )
    
main()
