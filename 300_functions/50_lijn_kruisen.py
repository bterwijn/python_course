'''Lijn Kruisen

Vraag de gebruiker om een lijn in een 2D assenstelsel gerepresenteerd
als een x,y punt en een stap grootte dx,dy. 


     |            o
     |           /.
     |          / . dy
     |         /  .
     |    x,y o . .
     |          dx
     |
  ---+--------------
     |

Met deze lijn representatie kunnen er een willekeurig aantal stappen
worden gezet in zowel positieve als negative richting.

Als deze lijn kruist met de X-as print dan op welke x-waarde deze lijn
de X-as kruist. Als deze lijn kruist met de Y-as print dan op welke
y-waarde deze lijn de Y-as kruist.

Bijvoorbeeld:

$ python crossing_lines.py
Geef een lijn gerepresenteerd als een x,y punt en een stap grootte dx,dy.
x: 2.5
y: 2
dx: 1
dy: 2
De lijn kruist de X-as op x: 1.5
De lijn kruist de Y-as op y: -3.0

'''

def bereken_kruising_x_as(x,y,dx,dy):
    if dy==0:
        return None
    a=-y/dy
    return x+a*dx

def bereken_kruising_y_as(x,y,dx,dy):
    if dx==0:
        return None
    a=-x/dx
    return y+a*dy

def main():
    print("Geef een lijn gerepresenteerd als een x,y punt en een stap grootte dx,dy.")
    x=float(input("x: "))
    y=float(input("y: "))
    dx=float(input("dx: "))
    dy=float(input("dy: "))
    kruising_x_as=bereken_kruising_x_as(x,y,dx,dy)
    if kruising_x_as is not None:
        print("De lijn kruist de X-as op x:",kruising_x_as)
    kruising_y_as=bereken_kruising_y_as(x,y,dx,dy)
    if kruising_y_as is not None:
        print("De lijn kruist de Y-as op y:",kruising_y_as)

main()
