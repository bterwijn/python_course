'''Regels

Vraag de gebruiker om de gewenste begin-lengte en print daarna een
regel met dat aantal '#' characters. Pas daarna de lengte steeds aan
door er random -1, 0, or 1 bij op te tellen en blijf de regels printen
maar stop het programma wanneer de lengte gelijk aan 0 is. De lengte
mag hierbij nooit groter dan 100 worden. Print aan het einde hoeveel
regels het programma heeft geprint.

Wat is de begin-lengte van de regel? 4
####
#####
####
####
###
##
###
##
#
Aantal regels: 9

'''
import random

def vraag_lengte():
    return int(input("Wat is de begin-lengte van de regel? "))

def beperk_lengte(lengte,max_lengte):
    return min(lengte,max_lengte)

def print_regel(lengte):
    print("#"*lengte)

def pas_lengte_aan(lengte):
    return lengte+random.randint(-1, 1)
    
def print_regels(lengte,max_lengte):
    regels=0
    while lengte>0:
        lengte=beperk_lengte(lengte,max_lengte)
        print_regel(lengte)
        regels+=1
        lengte=pas_lengte_aan(lengte)
    return regels

def main():
    max_lengte=100
    lengte=vraag_lengte()
    regels=print_regels(lengte,max_lengte)
    print("Aantal regels:", regels)

main()
