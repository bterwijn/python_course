'''Rij

Wat is de waarden van de som?:

 0 + 1 + 2 + 3 + ... + N

$ python rij.py
Tot welke waarde wil je een rij optellen? 20
De som is: 210

'''

def som_van_rij(N):
    som=0
    for i in range(N+1):
        som+=i
    return som
    

def main():
    N=int(input("Tot welke waarde wil je een rij optellen?"))
    print("De som is:", som_van_rij(N) )

main()
