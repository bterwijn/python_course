'''Dobbelsteen

Simuleer het gooien van een eerlijke dobbelsteen en bepaal het aantal
keer dat je moet gooien om 4 keer achter elkaar 6 te gooien. Doe dit
experiment vervolgens 1000 keer en bereken het gemiddelde aantal keer
gooien.

'''

import random

def aantal_gooien_tot(achter_elkaar_6):
    gooi=0
    achter_elkaar_6_teller=0
    while achter_elkaar_6_teller!=achter_elkaar_6:
        if random.randint(1,6)==6:
            achter_elkaar_6_teller+=1
        else:
            achter_elkaar_6_teller=0
        gooi+=1
    return gooi
    
def doe_experimenten(aantal_experimenten,achter_elkaar_6):
    gooi_som=0
    for i in range(aantal_experimenten):
        gooi_som+=aantal_gooien_tot(achter_elkaar_6)
    return gooi_som
        
def main():
    aantal_experimenten=1000
    achter_elkaar_6=4
    gooi_gemiddeld=doe_experimenten(aantal_experimenten,achter_elkaar_6)/aantal_experimenten
    print("gooi_gemiddeld:",gooi_gemiddeld)
    
main()
