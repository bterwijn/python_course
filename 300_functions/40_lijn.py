'''Lijn

Vraag de gebruiker om een lijn in een 2D assenstelsel gerepresenteerd
door een x,y beginpunt en een stap grootte dx,dy: 


     |            o
     |           /.
     |          / . dy
     |         /  .
     |    x,y o . .
     |          dx
     |
  ---+--------------
     |

Vraag daarbij hoe groot de stap is die de gebruiker wil zetten en
print op welk x,y punt het beginpunt dan uit komt na deze stap.

'''

def vraag_lijn():
    print("Geef een lijn in een 2D assenstelsel:")
    x=float(input("x: "))
    y=float(input("y: "))
    dx=float(input("dx: "))
    dy=float(input("dy: "))
    stappen=float(input("Hoeveel stappen wil je zetten? "))
    return x,y,dx,dy,stappen

def zet_stappen(x,y,dx,dy,stappen):
    return x+stappen*dx, y+stappen*dy

def main():
    print("Geef een lijn gerepresenteerd als een x,y punt en een stap grootte dx,dy.")
    x,y=zet_stappen( *vraag_lijn() )
    print("Dan kom je uit op (",x,",",y,")")

main()
