'''Print Rij

Schrijf de functie 'print_rij' die bij het uitvoeren van deze main
functie:

def main():
    print_rij()
    print_rij(5)
    print_rij(eind=15)
    print_rij(6,stap=2)
    print_rij(begin=3,stap=3)

de volgende output print:

0 1 2 3 4 5 6 7 8 9 10 
5 6 7 8 9 10 
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 
6 8 10 
3 6 9 

'''

def print_rij(begin=0,eind=10,stap=1):
    for i in range(begin,eind+1,stap):
        print(i,"",end='')
    print()

def main():
    print_rij()
    print_rij(5)
    print_rij(eind=15)
    print_rij(6,stap=2)
    print_rij(begin=3,stap=3)
    
main()
