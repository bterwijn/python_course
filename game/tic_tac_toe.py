import string
import random 

def x_to_letter(index):
    return string.ascii_lowercase[index]

def letter_to_x(letter):
    return ord(letter) - ord('a')

def move_to_coordinate(move):
    letter,y = move
    x = letter_to_x(letter)
    return x,y

def coordinate_to_move(x,y):
    return (x_to_letter(x),y)

class TicTacToe:

    def __init__(self,width,height,win_length):
        self.width=width
        self.height=height
        self.win_length=win_length
        self.board=[['.' for x in range(width)] for y in range(height) ]
        self.turn = 0

    def __repr__(self):
        s = ""
        for y,line in enumerate(self.board):
            s+=str(y)+''
            for cell in line:
                s+=cell
            s+='\n'
        s+=' '
        for x in string.ascii_lowercase[0:self.width]:
            s+=str(x)
        s+='\n'
        return s

    def get_turn(self):
        return self.turn
    
    def get_possible_moves(self):
        return [ coordinate_to_move(x,y)
                    for y,line in enumerate(self.board) 
                    for x,cell in enumerate(line) 
                    if cell == '.'] 

    def get_move_from_keyboard(self,moves):
        x_possibilities = sorted( {m[0] for m in moves} )
        while True:
            x = input(f"choose x ({''.join(x_possibilities)}): ")
            if x in x_possibilities:
                break
        y_possibilities = [str(m[1]) for m in moves if m[0]==x ]
        while True:    
            y = input(f"choose y ({''.join(y_possibilities)}): ")
            if y in y_possibilities:
                break
        return x,int(y)


    def do_move(self,move,icon):
        x,y = move_to_coordinate(move)
        self.board[y][x] = icon
        self.turn += 1

    def filter_winning_moves(self,moves,icon):
        return [m for m in moves if self.will_win(m,icon)]

    def get_random_move(self,moves,icon,oppenent_icon):
        winning_moves = self.filter_winning_moves(moves,icon)
        if len(winning_moves)>0:
            return random.choice(winning_moves)
        blocking_moves = self.filter_winning_moves(moves,oppenent_icon)
        if len(blocking_moves)>0:
            return random.choice(blocking_moves)
        return random.choice(moves)

    def is_won(self,move,icon):
        return self.check_lines(move,icon,icon)

    def will_win(self,move,icon):
        return self.check_lines(move,'.',icon)

    def check_lines(self,move,center,icon):
        x,y = move_to_coordinate(move)
        horizontal =  self.count_lines(x,y,center,icon, 1,0)
        vertical = self.count_lines(x,y,center,icon, 0,1)
        diagonal_up = self.count_lines(x,y,center,icon, 1,1)
        diagoval_down = self.count_lines(x,y,center,icon, 1,-1)
        # print(f"--- is_won move: {move} center: {center} icon: {icon}")
        # print("horizontal:",horizontal)
        # print("vertical:",vertical)
        # print("diagonal_up:",diagonal_up)
        # print("diagoval_down:",diagoval_down)
        win = horizontal >= self.win_length or \
                vertical >= self.win_length or \
                diagonal_up >= self.win_length or \
                diagoval_down >= self.win_length
        return win
    
    def count_lines(self,x,y,center,icon,dx,dy):
        if self.board[y][x] == center:
            count = 1
            count += self.count_line(x,y,icon,dx,dy)
            count += self.count_line(x,y,icon,-dx,-dy)
            return count
        return 0

    def count_line(self,x,y,icon,dx,dy):
        count=0
        tx,ty = x,y
        while True:
            tx+=dx
            ty+=dy
            if tx<0 or tx>=self.width or ty<0 or ty>=self.height:
                break
            if self.board[ty][tx] != icon:
                break
            count+=1
        return count
