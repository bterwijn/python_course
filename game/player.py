
class Player:

    def __init__(self,name,icon,ai_strength=0):
        self.name=name
        self.icon=icon
        self.ai_strength=ai_strength

    def __repr__(self):
        return f"name:{self.name} icon:{self.icon} ai_strength:{self.ai_strength}"

    def get_name(self):
        return self.name
    
    def get_icon(self):
        return self.icon
    
    def is_ai(self):
        return self.ai_strength>0

    def get_ai_strength(self):
        return self.ai_strength
