from tic_tac_toe import TicTacToe
from player import Player

def test_win_horizontal():
    game = TicTacToe(3,3,3)
    icon = 'X'
    m = ('a',2)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('c',2)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('b',2)
    assert game.will_win(m,icon) == True
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == True

def test_win_vertical():
    game = TicTacToe(3,3,3)
    icon = 'X'
    m = ('a',2)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('a',1)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('a',0)
    assert game.will_win(m,icon) == True
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == True

def test_win_diagonal_up():
    game = TicTacToe(3,3,3)
    icon = 'X'
    m = ('a',2)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('b',1)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('c',0)
    assert game.will_win(m,icon) == True
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == True

def test_win_diagonal_down():
    game = TicTacToe(3,3,3)
    icon = 'X'
    m = ('a',0)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('c',2)
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == False
    m = ('b',1)
    assert game.will_win(m,icon) == True
    game.do_move(m,icon)
    print(game)
    assert game.is_won(m,icon) == True
   
def test_will_win_moves():
    game = TicTacToe(3,3,3)
    icon = 'X'
    other_icon = 'O'
    m1 = ('a',0)
    game.do_move(m1,icon)
    m2 = ('b',0)
    game.do_move(m2,other_icon)
    m1 = ('a',1)
    game.do_move(m1,icon)
    m2 = ('b',1)
    game.do_move(m2,other_icon)
    moves = game.get_possible_moves()
    print(game)
    assert len(moves) == 5
    m1 = ('a',2)
    m2 = ('b',2)
    rand_move = game.get_random_move(moves,icon,other_icon)
    assert rand_move == m1
    rand_move = game.get_random_move(moves,other_icon,icon)
    assert rand_move == m2

def test_will_block_moves_block():
    game = TicTacToe(3,3,3)
    icon = 'X'
    other_icon = 'O'
    m = ('a',0)
    game.do_move(m,icon)
    m = ('a',1)
    game.do_move(m,icon)
    moves = game.get_possible_moves()
    assert len(moves) == 7
    win_move = ('a',2)
    assert game.will_win(win_move,icon)
    rand_move = game.get_random_move(moves,icon,other_icon)
    assert rand_move == win_move
    rand_move = game.get_random_move(moves,other_icon,icon)
    assert rand_move == win_move
