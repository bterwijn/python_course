import random
import copy

class MonteCarlo:

    def __init__(self):
        pass

    def get_best_move(self,game,players,current_icon,opponent_icon,repeats):
        move_evaluations = self.evaluate_moves(game,players,current_icon,repeats)
        print("move_evaluations:",move_evaluations)
        best_move,best_count = max(move_evaluations.items(),key=lambda kv : kv[1])
        return best_move

    def evaluate_moves(self,game,players,current_icon,repeats):
        moves = game.get_possible_moves()
        move_evaluations = {}
        for move in moves:
            game_copy = copy.deepcopy(game)
            game_copy.do_move(move,current_icon)
            print("=== move:",move)
            move_evaluations[move]=self.do_runs(game_copy,move,players,current_icon,repeats)
        return move_evaluations

    def do_runs(self,game,move,players,current_icon,repeats):
        counts=0
        if game.is_won(move,current_icon):
            counts = repeats
        else:
            for _ in range(repeats):
                game_copy = copy.deepcopy(game)
                result = self.do_single_run(game_copy,players,current_icon)
                counts += result
        return counts

    def do_single_run(self,game,players,win_icon):
        while True:
            current_player = players[ game.get_turn() % len(players) ]
            oppenent_player = players[ (game.get_turn()+1) % len(players) ]
            current_icon = current_player.get_icon()
            oppenent_icon = oppenent_player.get_icon()
            moves = game.get_possible_moves()
            if len(moves) == 0:
                return 0
            move = game.get_random_move(moves,current_icon,oppenent_icon)
            game.do_move(move,current_icon)
            if game.is_won(move,current_icon):
                if current_icon == win_icon:
                    return 1
                else:
                    return -1
