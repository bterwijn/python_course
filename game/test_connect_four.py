from connect_four import ConnectFour

def test_win_horizontal():
    game = ConnectFour(7,6,4)
    icon = 'X'
    move = 4
    for i in range(4):
        game.do_move(move,icon)
        print(game)
        assert game.is_won(move,icon) == (i==3)

def test_win_vertical():
    game = ConnectFour(7,6,4)
    icon = 'X'
    move = 1
    for i in range(4):
        game.do_move(move,icon)
        print(game)
        assert game.is_won(move,icon) == (i==3)
        move+=1

def test_win_diagonal_up():
    game = ConnectFour(7,6,4)
    icon = 'X'
    move = 1
    for i in range(4):
        for j in range(i):
            game.do_move(move,'O')
        game.do_move(move,icon)
        print(game)
        assert game.is_won(move,icon) == (i==3)
        move+=1

def test_win_diagonal_down():
    game = ConnectFour(7,6,4)
    icon = 'X'
    move = 5
    for i in range(4):
        for j in range(i):
            game.do_move(move,'O')
        game.do_move(move,icon)
        print(game)
        assert game.is_won(move,icon) == (i==3)
        move-=1

def test_will_win_moves():
    game = ConnectFour(7,6,4)
    icon1 = 'X'
    icon2 = 'O'
    for i in range(3):
        game.do_move(i,icon1)
        game.do_move(i,icon2)
    moves = game.get_possible_moves()
    print(game)
    assert len(moves) == 7
    assert game.will_win(3,icon1) == True
    rand_move = game.get_random_move(moves,icon1,icon2) # win 
    assert rand_move == 3
    rand_move = game.get_random_move(moves,icon2,icon1) # block
    assert rand_move == 3
    game.do_move(3,icon2)
    assert game.will_win(3,icon2) == True
    rand_move = game.get_random_move(moves,icon1,icon2) # block 
    assert rand_move == 3
    rand_move = game.get_random_move(moves,icon2,icon1) # win
    assert rand_move == 3
    

def test_will_win_moves():
    game = ConnectFour(7,6,4)
    icon1 = 'X'
    icon2 = 'O'
    for i in range(1,4):
        game.do_move(i,icon1)
        game.do_move(i,icon2)
    moves = game.get_possible_moves()
    rand_move = game.get_random_move(moves,icon1,icon2) # win 
    assert rand_move == 0 or rand_move == 4
    rand_move = game.get_random_move(moves,icon2,icon1) # block 
    assert rand_move == 0 or rand_move == 4
    game.do_move(0,icon2)
    rand_move = game.get_random_move(moves,icon1,icon2) # block win
    assert rand_move == 0 or rand_move == 4
    rand_move = game.get_random_move(moves,icon2,icon1) # block win
    assert rand_move == 0 or rand_move == 4
    game.do_move(0,icon1)
    rand_move = game.get_random_move(moves,icon1,icon2) # win 
    assert rand_move == 4
    rand_move = game.get_random_move(moves,icon2,icon1) # block 
    assert rand_move == 4