import random

from player import Player
from tic_tac_toe import TicTacToe
from connect_four import ConnectFour
from monte_carlo import MonteCarlo

def main():
    players = [ 
        Player("Bas",'X'),
        Player("AI",'O',200)
            ]
    #game = TicTacToe(3,3,3)
    game = ConnectFour(7,6,4)
    monte_carlo = MonteCarlo()
    print(game)
    while True:
        current_player = players[ game.get_turn() % len(players) ]
        oppenent_player = players[ (game.get_turn()+1) % len(players) ]
        current_icon = current_player.get_icon()
        oppenent_icon = oppenent_player.get_icon()
        print("current_player:",current_player)
        moves = game.get_possible_moves()
        if len(moves) == 0:
            break
        print("possible_moves:",moves)

        if current_player.is_ai():
            #move = game.get_random_move(moves,current_icon,oppenent_icon)
            move = monte_carlo.get_best_move(game,players,current_icon,oppenent_icon,current_player.get_ai_strength())
        else:
            print(f"Player {current_player.get_name()} what is your next move?")
            move = game.get_move_from_keyboard(moves)

        print("do move:",move)
        game.do_move(move,current_icon)
        print(game)
        if game.is_won(move,current_icon):
            print(f"player {current_player.get_name()} WON!")
            break


if __name__ == "__main__":
    main()
