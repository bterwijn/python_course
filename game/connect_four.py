import string
import random 

class ConnectFour:

    def __init__(self,width,height,win_length):
        self.width=width
        self.height=height
        self.win_length=win_length
        self.board=[['.' for x in range(width)] for y in range(height) ]
        self.turn = 0

    def __repr__(self):
        s = ""
        for y,line in enumerate(self.board):
            for cell in line:
                s+=' '+cell
            s+='\n'
        for x in range(0,self.width):
            s+=' '+str(x)
        s+='\n'
        return s

    def get_turn(self):
        return self.turn
    
    def get_possible_moves(self):
        return [ x
                for x,cell in enumerate(self.board[0]) 
                if cell == '.']

    def get_move_from_keyboard(self,moves):
        moves_str = [str(m) for m in moves]
        while True:
            x = input(f"choose x ({''.join(moves_str)}): ")
            if x in moves_str:
                break
        return int(x)

    def get_height(self,x):
        for y in range(0,self.height):
            if self.board[y][x] != '.':
                return y
        return self.height

    def do_move(self,move,icon):
        y = self.get_height(move) - 1
        self.board[y][move] = icon
        self.turn += 1

    def filter_winning_moves(self,moves,icon):
        return [m for m in moves if self.will_win(m,icon)]

    def get_random_move(self,moves,icon,oppenent_icon):
        winning_moves = self.filter_winning_moves(moves,icon)
        if len(winning_moves)>0:
            return random.choice(winning_moves)
        blocking_moves = self.filter_winning_moves(moves,oppenent_icon)
        if len(blocking_moves)>0:
            return random.choice(blocking_moves)
        return random.choice(moves)

    def is_won(self,move,icon):
        y = self.get_height(move)
        return self.check_lines(move,y,icon,icon)

    def will_win(self,move,icon):
        y = self.get_height(move) - 1
        return self.check_lines(move,y,'.',icon)

    def check_lines(self,x,y,center,icon):
        #print(f"check_lines x: {x} y: {y}")
        horizontal = self.count_lines(x,y,center,icon, 1,0)
        vertical = self.count_lines(x,y,center,icon, 0,1)
        diagonal_up = self.count_lines(x,y,center,icon, 1,1)
        diagoval_down = self.count_lines(x,y,center,icon, 1,-1)
        # print(f"--- is_won x: {x} y: {y} center: {center} icon: {icon}")
        # print("horizontal:",horizontal)
        # print("vertical:",vertical)
        # print("diagonal_up:",diagonal_up)
        # print("diagoval_down:",diagoval_down)
        win = horizontal >= self.win_length or \
                vertical >= self.win_length or \
                diagonal_up >= self.win_length or \
                diagoval_down >= self.win_length
        return win
    
    def count_lines(self,x,y,center,icon,dx,dy):
        #print(f"count_lines x: {x} y: {y} center:{center}")
        if y<self.height and self.board[y][x] == center:
            count = 1
            count += self.count_line(x,y,icon,dx,dy)
            count += self.count_line(x,y,icon,-dx,-dy)
            return count
        return 0

    def count_line(self,x,y,icon,dx,dy):
        count=0
        tx,ty = x,y
        while True:
            tx+=dx
            ty+=dy
            if tx<0 or tx>=self.width or ty<0 or ty>=self.height:
                break
            if self.board[ty][tx] != icon:
                break
            count+=1
        return count
