import random
import time
import sortedcontainers #pip install sortedcontainers

def random_element(size):
    return random.randint(0,size)

def make_list(size):
    return [ random_element(size) for i in range(size) ] # list

def make_set(size):
    return { random_element(size) for i in range(size) } # set

def make_sorted_list(size):
    sortedList=sortedcontainers.SortedList()
    for i in range(size):
        sortedList.add( random_element(size) )
    return sortedList

def search_elements(size,collection,tries):
    start_time=time.time()
    for i in range(tries):
        element=random_element(size)
        if element in collection:
            print("found",element)
        else:
            print("not found",element)
    stop_time=time.time()
    print("average search time:",(start_time-stop_time)/tries)
    
def main():
    size=100
    #collection=make_list(size)
    #collection=make_set(size)
    collection=make_sorted_list(size)
    print(collection)
    
    search_elements(size,collection,100)

            
main()
