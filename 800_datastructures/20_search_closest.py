
import sortedcontainers

def search_element(sortedList,element):
    print("-------------- search_element",element)
    print("found",sortedList.count(element),"occurrences of element: ",element)
    left_index=sortedList.bisect_left(element)-1
    if left_index>=0:
        print("first smaller element:",sortedList[left_index])
    right_index=sortedList.bisect_right(element)
    if right_index<len(sortedList):
        print("first larger element:",sortedList[right_index])

def main():
    sortedList=sortedcontainers.SortedList()
    sortedList.update([6,4,3,4,4,6])
    print(sortedList)
    
    search_element(sortedList,3)
    search_element(sortedList,4)
    search_element(sortedList,5)
    search_element(sortedList,6)
    
main()
