import math

# From: https://sudoku.com/easy/
sudoku_easy = [ [2, 6, 0, 0, 0, 3, 0, 1, 5 ],
                [4, 7, 0, 0, 0, 0, 0, 0, 8 ],
                [5, 8, 1, 0, 0, 4, 7, 6, 3 ],
                [0, 3, 0, 4, 8, 9, 0, 7, 0 ],
                [0, 0, 6, 0, 0, 2, 8, 3, 0 ],
                [0, 0, 8, 3, 1, 0, 0, 0, 0 ],
                [6, 9, 0, 0, 0, 8, 0, 0, 7 ],
                [3, 0, 0, 0, 9, 0, 2, 0, 0 ],
                [0, 1, 0, 5, 0, 0, 0, 9, 6 ] ]

# sudoku_easy but with more open cells
sudoku_multi_solutions = [ [2, 0, 0, 0, 0, 3, 0, 1, 5 ],
                           [4, 7, 0, 0, 0, 0, 0, 0, 8 ],
                           [0, 8, 1, 0, 0, 4, 7, 0, 3 ],
                           [0, 3, 0, 4, 8, 9, 0, 0, 0 ],
                           [0, 0, 6, 0, 0, 0, 8, 3, 0 ],
                           [0, 0, 8, 3, 1, 0, 0, 0, 0 ],
                           [6, 9, 0, 0, 0, 8, 0, 0, 7 ],
                           [3, 0, 0, 0, 9, 0, 2, 0, 0 ],
                           [0, 1, 0, 5, 0, 0, 0, 9, 6 ] ]

# In 2012, Finnish mathematician Arto Inkala claimed to have created the "World's Hardest Sudoku".
sudoku_hard = [ [8, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [0, 0, 3, 6, 0, 0, 0, 0, 0 ],
                [0, 7, 0, 0, 9, 0, 2, 0, 0 ],
                [0, 5, 0, 0, 0, 7, 0, 0, 0 ],
                [0, 0, 0, 0, 4, 5, 7, 0, 0 ],
                [0, 0, 0, 1, 0, 0, 0, 3, 0 ],
                [0, 0, 1, 0, 0, 0, 0, 6, 8 ],
                [0, 0, 8, 5, 0, 0, 0, 1, 0 ],
                [0, 9, 0, 0, 0, 0, 4, 0, 0 ] ]

def print_sudoku(sudoku):
    for r in sudoku:
        for c in r:
            print( ' '+str(c) if c>0 else '  ',end='')
        print()

def get_open_cells(sudoku):
    open_cells=[]
    for r,row in enumerate(sudoku):
        for c,value in enumerate(row):
            if value==0:
                open_cells.append((r,c))
    return open_cells

def set_value(sudoku,coord,value):
    sudoku[coord[0]][coord[1]]=value

def get_row_values(sudoku,row):
    return sudoku[row]

def get_column_values(sudoku,col):
    return [row[col] for row in sudoku]

def get_block_values(sudoku,coord):
    values=[]
    block_size=round(math.sqrt(len(sudoku)))
    block_r=(coord[0]//block_size)*block_size
    block_c=(coord[1]//block_size)*block_size
    for r in range(block_size):
        for c in range(block_size):
            values.append(sudoku[block_r+r][block_c+c])
    return values

def remove_values(values,minus):
    for i in minus:
        try:
            values.remove(i)
        except ValueError:
            pass
    return values

def get_valid_values(sudoku,coord):
    valid=[i for i in range(1,len(sudoku)+1)]
    valid=remove_values(valid, get_row_values(sudoku,   coord[0]) )
    valid=remove_values(valid, get_column_values(sudoku,coord[1]) )
    valid=remove_values(valid, get_block_values(sudoku, coord )   )
    return valid

def solve_sudoku(sudoku):
    coords=get_open_cells(sudoku)
    if len(coords)==0:
        print("solution:")
        print_sudoku( sudoku )
    else:
        coord=coords[0]
        for value in get_valid_values(sudoku,coord):
            set_value(sudoku,coord,value)
            solve_sudoku(sudoku)
            set_value(sudoku,coord,0)

print_sudoku( sudoku_easy )
print( get_open_cells(sudoku_easy) )
print( get_valid_values(sudoku_easy,(0,2)) )

solve_sudoku( sudoku_multi_solutions )
