'''Vierkanten

Een vierkant met lengte 5 heeft een oppervlakte van 5*5=25. Print de
lengte van alle vierkanten die een oppervlakte hebben tussen de 100 en
2500 (dus niet inclusief 100 en 2500). Beschouw daarbij alleen
vierkanten met een geheel getal als lengte.

$ python vierkanten.py
lengte: 11
lengte: 12
lengte: 13
...

'''
import math

min_oppervlakte = 100
max_oppervlakte = 2500

max_lengte = math.ceil( math.sqrt(max_oppervlakte) )
for lengte in range(1,max_lengte+1 ):
    oppervlakte = lengte*lengte
    if oppervlakte > min_oppervlakte and oppervlakte < max_oppervlakte:
        print("lengte:",lengte)
        
print("\nof:\n")

min_lengte = math.ceil(  math.sqrt(min_oppervlakte) )
if min_lengte*min_lengte <= min_oppervlakte:
    min_lengte+=1
max_lengte = math.floor( math.sqrt(max_oppervlakte) )
if max_lengte*max_lengte >= max_oppervlakte:
    max_lengte-=1
for lengte in range( min_lengte, max_lengte+1 ):
    print("lengte:",lengte)
