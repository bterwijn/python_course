'''Speerwerpen

Een speerwerpster gooit een afstand tussen de 50 en 70 meter waarbij
elke afstand even waarschijnlijk is (uniforme verdeling). Simuleer
10000 keer gooien van de speerwerpster en print na elke keer gooien de
gemiddelde afstand van alle voorgaande worpen (voortschrijdend
gemiddelde).

'''

import random

min_afstand=50
max_afstand=70
som_afstand=0
aantal_worpen=10000
for worp in range(aantal_worpen):
    afstand=random.uniform(min_afstand, max_afstand)
    som_afstand+=afstand
    gemiddelde_afstand=som_afstand/(worp+1)
    print("worp:",worp,"gemiddelde_afstand:",gemiddelde_afstand)

