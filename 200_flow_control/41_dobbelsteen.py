'''Dobbelsteen

Simuleer het gooien van een eerlijke dobbelsteen en bepaal het aantal
keer dat je moet gooien om 4 keer achter elkaar 6 te gooien. Doe dit
experiment vervolgens 1000 keer en bereken het gemiddelde aantal keer
gooien.

'''

import random

aantal_experimenten=1000
achter_elkaar_6=4
gooi_som=0;
for i in range(aantal_experimenten):
    gooi=0
    achter_elkaar_6_teller=0
    while achter_elkaar_6_teller!=achter_elkaar_6:
        dobbelsteen=random.randint(1,6)
        gooi+=1
        if dobbelsteen==6:
            achter_elkaar_6_teller+=1
        else:
            achter_elkaar_6_teller=0
    #print("gooi:",gooi)
    gooi_som+=gooi
gooi_gemiddeld=gooi_som/aantal_experimenten
print("gooi_gemiddeld:",gooi_gemiddeld)
