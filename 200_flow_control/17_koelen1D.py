'''Koelen 1D

In een koelsysteem willen we een zo laag mogelijke temperatuur
bereiken. Om dat te bereiken kunnen we 0 t\m 99 gram koelvloeistof x
toevoegen. De temperatuur die bereikt wordt met een bepaalde
hoeveelheid koelvloeistof is gegeven als:

  temperatuur = 0.1*x**2 -8*x +45

Wat is de laagste temperatuur die we kunnen bereiken en hoeveel
gram koelvloeistof x moeten we hiervoor toevoegen?
'''


min_temperatuur=float("inf")
best_x=0
for x in range(100):
    temperatuur = 0.1*x**2 -8*x +45
    if temperatuur<min_temperatuur:
        min_temperatuur=temperatuur
        best_x=x
print("min_temperatuur:",min_temperatuur)
print("best_x:",best_x)
