'''Dobbelsteen

Simuleer het 1 miljoen keer gooien van een eerlijke dobbelsteen en
bepaal het maximale aantal keren achter elkaar 6 gooien.

'''

import random

n=10**6
achter_elkaar_6_teller=0
max_achter_elkaar_6_teller=0
for i in range(n):
    dobbelsteen=random.randint(1,6)
    if dobbelsteen==6:
        achter_elkaar_6_teller+=1
        if achter_elkaar_6_teller>max_achter_elkaar_6_teller:
            max_achter_elkaar_6_teller=achter_elkaar_6_teller
    else:
        achter_elkaar_6_teller=0
print("max_achter_elkaar_6_teller:",max_achter_elkaar_6_teller)
