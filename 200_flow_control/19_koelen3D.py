'''Koelen 2D

In een koelsysteem willen we een zo laag mogelijke temperatuur
bereiken. Om dat te bereiken kunnen we 0 t\m 99 gram koelvloeistof x
en 0 t\m 99 gram koelvloeistof y en 0 t\m 99 gram koelvloeistof z
toevoegen. De temperatuur die bereikt wordt met een bepaalde
hoeveelheid koelvloeistof is gegeven als:

  temperatuur = +0.01*z**2 -0.02*z*y -0.01*z*x +0.1*x**2 +0.02*y**2 -8*x -0.2*y +5

Wat is de laagste temperatuur die we kunnen bereiken en hoeveel
gram koelvloeistof x,y en z moeten we hiervoor toevoegen?

'''

min_temperatuur=float("inf")
best_x=0
best_y=0
best_z=0
for x in range(100):
    for y in range(100):
        for z in range(100):
            temperatuur = +0.01*z**2 -0.02*z*y -0.01*z*x +0.1*x**2 +0.02*y**2 -8*x -0.2*y +5
            if temperatuur<min_temperatuur:
                min_temperatuur=temperatuur
                best_x=x
                best_y=y
                best_z=z
print("min_temperatuur:",min_temperatuur)
print("best_x:",best_x)
print("best_y:",best_y)
print("best_z:",best_z)
