'''Regels

Vraag de gebruiker om de gewenste begin-lengte en print daarna een
regel met dat aantal '#' characters. Pas daarna de lengte steeds aan
door er random -1, 0, or 1 bij op te tellen en blijf de regels printen
maar stop het programma wanneer de lengte gelijk aan 0 is. De lengte
mag hierbij nooit groter dan 100 worden. Print aan het einde hoeveel
regels het programma heeft geprint.

Wat is de begin-lengte van de regel? 4
####
#####
####
####
###
##
###
##
#
Aantal regels: 9

'''
import random

max_lengte=100
lengte=int(input("Wat is de begin-lengte van de regel? "))
regels=0
while lengte>0:
    if lengte > max_lengte:
        lengte = max_lengte
    for l in range(lengte):
        print("#",end="")
    print()
    regels+=1
    lengte+=random.randint(-1, 1)
print("Aantal regels:",regels)

'''
# of
max_lengte=100
lengte=int(input("Wat is de begin-lengte van de regel? "))
regels=0
while lengte>0:
    lengte = min(lengte,max_lengte)
    print("#"*lengte)
    regels+=1
    lengte+=random.randint(-1, 1)
print("Aantal regels:",regels)
'''
