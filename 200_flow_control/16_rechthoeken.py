'''Rechthoeken

Een rechthoek met hoogte 5 en breedte 8 heeft een oppervlakte van
5*8=40. Print de hoogte en breedte van alle rechthoeken die een
oppervlakte hebben tussen de 100 en 110 (dus niet inclusief 100 en
110). Beschouw daarbij alleen rechthoeken met een geheel getal als
hoogte en breedte.

$ python rechthoeken.py
hoogte: 1 breedte: 101
hoogte: 1 breedte: 102
hoogte: 1 breedte: 103
...

'''
import math

min_oppervlakte = 100
max_oppervlakte = 110

max_lengte = max_oppervlakte
for hoogte in range(1, max_lengte+1):
    for breedte in range(1, max_lengte+1):
        oppervlakte = hoogte*breedte
        if oppervlakte > min_oppervlakte and oppervlakte < max_oppervlakte:
            print("hoogte:",hoogte,"breedte:",breedte)
 
print("\nof:\n")

max_lengte = max_oppervlakte
for hoogte in range(1, max_lengte+1):
    min_breedte =  math.ceil(  min_oppervlakte/hoogte  )
    max_breedte =  math.floor( max_oppervlakte/hoogte  )
    for breedte in range(min_breedte, max_breedte+1):
        oppervlakte = hoogte*breedte
        if oppervlakte > min_oppervlakte and oppervlakte < max_oppervlakte:
            print("hoogte:",hoogte,"breedte:",breedte)
