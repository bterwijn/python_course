'''Rij

Wat is de waarden van de som?:

 0 + 1 + 2 + 3 + ... + 20


$ python rij.py
De som is: 210

'''

som=0
for i in range(20+1):
    som+=i
print("De som is:",som)


'''
kan eventueel ook met wiskunde uitgerekend worden:

 1 + 2 + 3 + ... + 20 = 20*(20+1)/2 = 210 (zie rekenkundige rij)

'''
