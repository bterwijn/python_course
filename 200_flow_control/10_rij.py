'''Rij

Wat is de kleinste waarde van N waarvoor de som van de rij:

 0 + 1 + 2 + 3 + ... + N

groter is dan 1000?

$ python rij.py
De kleinste waarde van N waarvoor de som van de rij groter is dan 1000 is: <antwoord>

'''

max_rij=1000
N=0
som=0
while som<max_rij:
    N+=1
    som+=N
print("De kleinste waarde van N waarvoor de som van de rij groter is dan 1000 is:",N)

'''
kan eventueel met wiskunde opgelost worden:

 1 + 2 + 3 + ... + N = N*(N+1)/2  (zie rekenkundige rij)
 
 N*(N+1)/2       = 1000
 N*(N+1)         = 2000
 N**2 + N - 2000 = 0
 
 ABC-formule met a=1 b=1 c=-2000
 N = (-b +-math.sqrt( b**2 - 4ac )) / (2a)
 N = (-1 +-math.sqrt( 8001       )) /  2
 N = 44.224154547626725

dus de kleinste waarde van N waarvoor de som van de rij groter is dan 1000 is 45
'''
